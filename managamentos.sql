-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 30 Lip 2016, 10:59
-- Wersja serwera: 5.7.11
-- Wersja PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `managamentos`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `historia_logowania`
--

CREATE TABLE `historia_logowania` (
  `user_id_osoby` int(10) DEFAULT NULL,
  `data_logowania` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `historia_logowania`
--

INSERT INTO `historia_logowania` (`user_id_osoby`, `data_logowania`) VALUES
(1, '2016-07-30 12:55:11'),
(1, '2016-07-30 12:55:11'),
(1, '2016-07-30 12:58:25'),
(1, '2016-07-30 12:58:25');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `historia_odpowiedzialnosci`
--

CREATE TABLE `historia_odpowiedzialnosci` (
  `zadania_id_zadania` int(10) DEFAULT NULL,
  `user_id_osoby` int(10) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `historia_realizacji`
--

CREATE TABLE `historia_realizacji` (
  `zadania_id_zadania` int(10) DEFAULT NULL,
  `user_id_osoby` int(10) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kamienie_milowe`
--

CREATE TABLE `kamienie_milowe` (
  `id_kamienie_milowe` int(10) NOT NULL,
  `nazwa` varchar(30) DEFAULT NULL,
  `projekt_id_projekt` int(10) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `odpowiedzialnosc` varchar(60) DEFAULT NULL,
  `postep` varchar(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `projekt`
--

CREATE TABLE `projekt` (
  `id_projekt` int(10) NOT NULL,
  `nazwa` varchar(30) DEFAULT NULL,
  `aktywnosc` varchar(30) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `projekt`
--

INSERT INTO `projekt` (`id_projekt`, `nazwa`, `aktywnosc`, `date_from`, `date_to`) VALUES
(1, 'projekt', 'Aktywny', '2016-07-30', '2016-07-31');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id_osoby` int(10) NOT NULL,
  `imie` varchar(30) DEFAULT NULL,
  `nazwisko` varchar(30) DEFAULT NULL,
  `login` varchar(30) DEFAULT NULL,
  `haslo` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `uprawnienia` varchar(30) DEFAULT NULL,
  `pracownik_ukonczone` int(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id_osoby`, `imie`, `nazwisko`, `login`, `haslo`, `email`, `uprawnienia`, `pracownik_ukonczone`) VALUES
(1, 'admin', 'admin', 'admin', 'admin', 'admin', 'admin', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zadania`
--

CREATE TABLE `zadania` (
  `id_zadania` int(10) NOT NULL,
  `projekt_id_projekt` int(10) DEFAULT NULL,
  `nazwa_zadania` varchar(30) DEFAULT NULL,
  `kto_wykonuje` varchar(150) DEFAULT NULL,
  `wrealizacji` varchar(30) DEFAULT NULL,
  `priorytet` varchar(30) DEFAULT NULL,
  `data_from` date DEFAULT NULL,
  `data_to` date DEFAULT NULL,
  `procentowe_ukonczenie` varchar(4) DEFAULT NULL,
  `notatka` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `historia_odpowiedzialnosci`
--
ALTER TABLE `historia_odpowiedzialnosci`
  ADD KEY `zadania_id_zadania` (`zadania_id_zadania`),
  ADD KEY `user_id_osoby` (`user_id_osoby`);

--
-- Indexes for table `historia_realizacji`
--
ALTER TABLE `historia_realizacji`
  ADD KEY `zadania_id_zadania` (`zadania_id_zadania`),
  ADD KEY `user_id_osoby` (`user_id_osoby`);

--
-- Indexes for table `kamienie_milowe`
--
ALTER TABLE `kamienie_milowe`
  ADD PRIMARY KEY (`id_kamienie_milowe`),
  ADD KEY `projekt_id_projekt` (`projekt_id_projekt`);

--
-- Indexes for table `projekt`
--
ALTER TABLE `projekt`
  ADD PRIMARY KEY (`id_projekt`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_osoby`);

--
-- Indexes for table `zadania`
--
ALTER TABLE `zadania`
  ADD PRIMARY KEY (`id_zadania`),
  ADD KEY `projekt_id_projekt` (`projekt_id_projekt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kamienie_milowe`
--
ALTER TABLE `kamienie_milowe`
  MODIFY `id_kamienie_milowe` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `projekt`
--
ALTER TABLE `projekt`
  MODIFY `id_projekt` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id_osoby` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `zadania`
--
ALTER TABLE `zadania`
  MODIFY `id_zadania` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
