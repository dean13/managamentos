package Baza;

import Generic.ActiveUser;
import Generic.MilestoneGen;
import Generic.NewUser;
import Generic.ProjectGen;
import Generic.TablePerson;
import Generic.TaskGen;
import Powiadomienia.Powiadomienia;
import java.sql.*;
import java.io.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;

/**
 * Klasa odpowiadająca za połaczenie miedzy aplikacją i bazą danych. Zawiera
 * niezbędne metody aby aplikacja działa poprawnie
 *
 * @author Kamil W, Alan U, Krzysztof Ś
 */
public class Connect {

    /**
     * Statyczna zmienna connection, obiekt klasy Connection
     */
    public static Connection connection;
    /**
     * Statyczna zmienna statement, obiekt klasy Statement, przechowująca
     * zapytanie SQL
     */
    public static Statement statement;
    /**
     * Statyczna zmienna logincorrect, typu String, przechowująca wartość czy
     * login jest poprawny
     */
    public static String logincorrect;
    /**
     * Statyczna zmienna resultSet, obiekt klasy ResultSet
     */
    public static ResultSet resultSet;
    /**
     * Statyczna zmienna rsmd, obiekt klasy ResultSetMetaData zwracająca
     * zawartość tabeli
     */
    public static ResultSetMetaData rsmd;
    /**
     * Statyczna zmienna login, typu String przechowująca nazwę zalogowanego
     * użytkownika
     */
    public static String login;
    /**
     * Statyczna zmienna password, typu String przechowująca hasło zalogowanego
     * użytkownika
     */
    public static String password;
    /**
     * Statyczna zmienna uprawnienia, typu String przechowująca uprawnienia
     * zalogowanego użytkownika
     */
    public static String uprawnnienia;
    /**
     * Statyczna zmienna id_osoby, typu String przechowująca id zalogowanego
     * użytkownika
     */
    public static String id_osoby;
    /**
     * Prywatna finalna zmienna activeUser, przechowująca aktualnie zalogowanego
     * użytkownika
     */
    private final String activeUser = new ActiveUser().getLogin();
    /**
     * Prywatna finalna zmienna permissions, przechowująca uprawnienia
     * zalogowanego użytkownika
     */
    private final String permissions = new ActiveUser().getUprawnienia();
    /**
     * Instancja do klasy Powiadomienia
     */
    private Powiadomienia pow;
    private static Powiadomienia pow2;
    
    private static ResourceBundle resourceBundle;
    
    
     public static boolean connectserverfirst(String adres,String nazwa,String login,String haslo) {
        
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            pow2.errorNotification("Nie odnaleziono sterownika JDBC!");
        }
        try {
            System.out.println("jdbc:mysql://"+adres+"/"+nazwa+"?characterEncoding=utf-8");
            System.out.println(adres+nazwa+login+haslo);
            connection = DriverManager.getConnection("jdbc:mysql://"+adres+"/"+nazwa+"?characterEncoding=utf-8", login, haslo);
            System.out.println("jdbc:mysql://"+adres+"/"+nazwa+"?characterEncoding=utf-8");
            //statement = connection.createStatement();
            logincorrect = "true";
            return true;
        } catch (SQLException ex) {
            pow2.errorNotification("Wystąpił błąd podczas próby nawiązania połączenia z bazą danych!!!");
            logincorrect = "false";
            return false;
        }
    }

    public Connect (ResourceBundle rb) {
        Connect.resourceBundle = rb;
        Connect.pow2 = new Powiadomienia(rb);
        this.pow = new Powiadomienia(rb);
    }
    
    public Connect () {
        
    }
    
    /**
     * Metoda odpowiadająca za połączenia z bazą danych
     *
     * @return - zwraca flagę informującą o stanie połączenia
     * 
     */
    public static boolean connectserver() { 
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            pow2.errorNotification(resourceBundle.getString("errorJDBC"));
        }

        try {     
            String a,b,c,d;
            String sciezka;
            FileReader plik;
            BufferedReader reader;

            //nazwa pliku
            sciezka="properties.SaveProperties.HOMESOURCE+\"danedostepowe\\\\adres.txt";
            //tworzenie i zapis do pliku adress
            plik=new FileReader(sciezka);
            reader=new BufferedReader(plik);
            a=reader.readLine();
            
            //------------------------zapis do nazwabazy
           
            //nazwa pliku
            sciezka="properties.SaveProperties.HOMESOURCE+\"danedostepowe\\\\nazwabazy.txt";
            plik=new FileReader(sciezka);
            reader=new BufferedReader(plik);
            b=reader.readLine();

           
            
            //------------------------zapis do login
            
            //nazwa pliku
            sciezka="properties.SaveProperties.HOMESOURCE+\"danedostepowe\\\\login.txt";
            //tworzenie i zapis do pliku adress
            plik=new FileReader(sciezka);
            reader=new BufferedReader(plik);
            c=reader.readLine();
           
             
            //------------------zapis haslo 
             
             
            //nazwa pliku
            sciezka="properties.SaveProperties.HOMESOURCE+\"danedostepowe\\\\haslo.txt";
            //tworzenie i zapis do pliku adress
            plik=new FileReader(sciezka);
            reader=new BufferedReader(plik);
            d=reader.readLine();
            connection = DriverManager.getConnection("jdbc:mysql://"+a+"/"+b+"?characterEncoding=utf-8", c, d);
            //statement = connection.createStatement();
            logincorrect = "true";
            return true;
        } catch (SQLException ex)  {
            pow2.errorNotification(resourceBundle.getString("connect"));
            logincorrect = "false";
            return false;
        }
        catch(FileNotFoundException z )
        {
            
        }
        catch(IOException w)
        {
            
        }
        return false;
    }

    public static void createStatement() {
        try {
            statement = connection.createStatement();
        } catch (SQLException ex) {
            pow2.errorNotification(resourceBundle.getString("problemWith"));
        }
    }

    public static void closeStatement() {
        try {
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metoda odpowiedzialna za rozłączenie z bazą danych
     */
    public static void disconnnect() {
        try {
            connection.close();
            statement.close();
        } catch (SQLException ex) {
            pow2.warningNotification(resourceBundle.getString("closeConnect"));
        }
    }

    /**
     * Metoda do sprawdzania kto jest logowany
     *
     * @param user - login
     * @param pass - hasło
     */
    public static void checkUser(String user, String pass) {

        //nie dodawac polaczania i  rozlaczania z baza
        try {
            createStatement();
            resultSet = statement.executeQuery("SELECT login,haslo,uprawnienia,id_osoby FROM user WHERE login ='" + user + "' and haslo='" + pass + "';");
            rsmd = resultSet.getMetaData();
            while (resultSet.next()) {
                login = resultSet.getString("login");
                password = resultSet.getString("haslo");
                uprawnnienia = resultSet.getString("uprawnienia");
                id_osoby = resultSet.getString("id_osoby");
            }
            closeStatement();
        } catch (SQLException ex) {
            pow2.errorNotification(resourceBundle.getString("loginProblem"));
        } catch (NullPointerException ex) {
        }
    }

    /**
     * Metoda Delete(usuń) wysyłająca zapytanie do bazy danch
     *
     * @param zapytanie -treść zapytania
     *
     */
    public void Delete(String zapytanie) {
        createStatement();
        try {
            statement = connection.createStatement();
            statement.execute(zapytanie);
        } catch (SQLException ex) {
            pow.warningNotification(resourceBundle.getString("deleteProblem"));
        }
        closeStatement();
    }

    public void UpdatePerson(String imie, String nazwisko, String login, String haslo, String email, String uprawnienia, String loginWhere) {
        createStatement();
        try {
            // create our java preparedstatement using a sql update query
            PreparedStatement ps = connection.prepareStatement(
                    "UPDATE user SET imie = ?, nazwisko = ?, login = ?, haslo = ?, email = ?, uprawnienia = ?  WHERE login = ? ");
            // set the preparedstatement parameters
            ps.setString(1, imie);
            ps.setString(2, nazwisko);
            ps.setString(3, login);
            ps.setString(4, haslo);
            ps.setString(5, email);
            ps.setString(6, uprawnienia);
            ps.setString(7, loginWhere);
            // call executeUpdate to execute our sql update statement
            ps.executeUpdate();
            ps.close();
            pow.informationNotification(resourceBundle.getString("updateOK"));
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeStatement();
    }

    /**
     * Metoda update(aktualizuj) wysyłająca zapytanie do bazy danych
     *
     * @param zapytanie -treśc zapytania
     * @throws java.sql.SQLException - przekazanie wyjątku klasę wyżej
     */
    public void Update(String zapytanie) throws SQLException {
        statement = connection.createStatement();
        statement.executeUpdate(zapytanie);
        statement.close();
    }

    /**
     * Metoda insert(dodaj rekord) wysyłająca zapytanie do bazy danych
     *
     * @param name - nazwa projektu
     * @param activiti - status projektu
     * @param startDate - data rozpoczęcia
     * @param endDate - data zakonczenia
     *
     * @throws java.io.IOException - przekazanie wyjątku do klasy Project
     */
    public void Insert(String name, String activiti, LocalDate startDate, LocalDate endDate) throws IOException {
        createStatement();
        String startLocal = startDate.toString();
        String endLocal = endDate.toString();
        try {
            // zapytanie insertu do tabeli projekty
            String query = " insert into projekt (nazwa, aktywnosc, date_from, date_to)"
                    + " values (?, ?, ?, ?)";
            // preparedStatement  z argumentem zapytania query
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, name);
            preparedStmt.setString(2, activiti);
            preparedStmt.setString(3, startLocal);
            preparedStmt.setString(4, endLocal);
            // wykonanie zapytania
            preparedStmt.execute();
        } catch (SQLException ex) {
            throw new IOException("insertToProject");
        }
        closeStatement();
    }

    /**
     * Metoda EasyInsert do prostego zapytania select
     *
     * @param query - zapytanie
     * @throws Exception - przekazanie wyjątku klasę wyżej
     */
    public static void EasyInsert(String query) throws Exception {
        statement = connection.createStatement();
        statement.execute(query);
        statement.close();
    }

    public static void EasyInsert1(String query) throws Exception {
        // nie dodawac polaczenia i rozlaczenia
        statement = connection.createStatement();
        statement.execute(query);
        statement.close();
    }

    /**
     * Metoda InsertPerson do dodawania rekordów dla tabeli Osoby
     *
     * @param name - imię
     * @param surname - nazwisko
     * @param login - login
     * @param password - hasło
     * @param email - emain
     * @param funkction - funkcja
     */
    public void InsertPerson(String name, String surname, String login, String password, String email, String funkction) {
        createStatement();
        try {
            // zapytanie insertu do tabeli projekty
            String query = " insert into user (imie, nazwisko, login, haslo, email, uprawnienia)"
                    + " values (?, ?, ?, ?, ?, ?)";
            // preparedStatement  z argumentem zapytania query
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            preparedStmt.setString(1, name);
            preparedStmt.setString(2, surname);
            preparedStmt.setString(3, login);
            preparedStmt.setString(4, password);
            preparedStmt.setString(5, email);
            preparedStmt.setString(6, funkction);
            // wykonanie zapytania
            preparedStmt.execute();
            pow.informationNotification(resourceBundle.getString("addNewUser")+" " + funkction);
        } catch (SQLException ex) {
            pow.warningNotification(resourceBundle.getString("errorAddPerson"));
        }
        closeStatement();
    }

    /**
     * Metoda wyciągająca z tabeli Projekt (id_projekt i nazwę) i zwraca jako
     * mapę.
     *
     * @param zapytanie - zapytanie
     * @return - zwraca mapę id_projekt - klucz, nazwa - wartość
     * @throws SQLException - wyjątek SQL
     *
     */
    public Map<String, String> select(String zapytanie) throws SQLException {
        createStatement();
        Map<String, String> map = new HashMap<>();
        resultSet = statement.executeQuery(zapytanie);
        while (resultSet.next()) {
            String id = resultSet.getString("id_projekt");
            String name = resultSet.getString("nazwa");
            map.put(id, name);
        }
        closeStatement();
        return map;
    }

    /**
     * Metoda zwracająca osoby z tebli Osoba
     *
     * @param zapytanie - treść zapytania
     * @return - zwraca ObservableList klasy TablePerson
     * @throws SQLException - przekazanie wyjątku klasę wyżej
     */
    public ObservableList SelectTablePerson(String zapytanie) throws SQLException {
        createStatement();
        ObservableList<TablePerson> obsDataPerson = FXCollections.observableArrayList();
        resultSet = statement.executeQuery(zapytanie);
        while (resultSet.next()) {
            String log = resultSet.getString("login");
            String nazwisko = resultSet.getString("nazwisko");
            String data = resultSet.getString("data_logowania");
            // wywolanie konstruktora z parametrami
            TablePerson tabP = new TablePerson(log, nazwisko, data);
            obsDataPerson.add(tabP);
        }
        closeStatement();
        return obsDataPerson;
    }

    /**
     *
     * @param zapytanie
     * @return
     * @throws SQLException Druga tabela w zakldace osoby
     */
    public ObservableList SelectTablePerson1(String zapytanie) throws SQLException {
        createStatement();
        ObservableList<NewUser> obsDataPerson = FXCollections.observableArrayList();
        resultSet = statement.executeQuery(zapytanie);
        while (resultSet.next()) {
            String imie = resultSet.getString("imie");
            String nazwisko = resultSet.getString("nazwisko");
            String login = resultSet.getString("login");
            String email = resultSet.getString("email");
            String uprawnienia = resultSet.getString("uprawnienia");
            // wywolanie konstruktora z parametrami
            NewUser tabP = new NewUser(imie, nazwisko, login, email, uprawnienia);
            obsDataPerson.add(tabP);
        }
        closeStatement();
        return obsDataPerson;
    }

    public String Select(String zapytanie) {
        createStatement();
        String wynik = "null";
        try {
            resultSet = statement.executeQuery(zapytanie);
            while (resultSet.next()) {
                wynik = resultSet.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeStatement();
        return wynik;
    }

    /**
     * Metoda pobierająca dane z tabel.
     *
     * @param zapytanie - zapytanie SQL
     * @param tabela - nazwa tabeli
     * @return - zwraca ObservableList klasy generycznej zależnej od tabeli.
     * @throws SQLException - przekazanie wyjątku klasę wyżej
     */
    public ObservableList selectTable(String zapytanie, String tabela) throws SQLException {
        createStatement();
        ObservableList<TaskGen> obsDataTask = FXCollections.observableArrayList();
        ObservableList<ProjectGen> obsDataProject = FXCollections.observableArrayList();
        ObservableList<String> obsDataUser = FXCollections.observableArrayList();
        ObservableList<String> obsNote = FXCollections.observableArrayList();
        ObservableList<String> obsTitle = FXCollections.observableArrayList();
        ObservableList<MilestoneGen> obsDataMilestone = FXCollections.observableArrayList();
        ObservableList<LocalDate> obsDateProject = FXCollections.observableArrayList();
        resultSet = statement.executeQuery(zapytanie);
        switch (tabela) {
            case "zadania":
                while (resultSet.next()) {
                    String title = resultSet.getString("nazwa_zadania");
                    String whoDoit = resultSet.getString("kto_wykonuje");
                    String Implementation = resultSet.getString("wrealizacji");
                    String priorytet = resultSet.getString("priorytet");
                    String dataFrom = resultSet.getString("data_from");
                    String dataTo = resultSet.getString("data_to");
                    int procentTask = resultSet.getInt("procentowe_ukonczenie");
                    // wywolanie konstruktora z parametrami
                    TaskGen taskG = new TaskGen(title, whoDoit, priorytet, Implementation, dataFrom, dataTo, procentTask);
                    obsDataTask.add(taskG);
                }
                return obsDataTask;
            case "projekt":
                while (resultSet.next()) {
                    //iterowanie wierszy
                    String name = resultSet.getString("nazwa");
                    String state = resultSet.getString("aktywnosc");
                    LocalDate dateStart = resultSet.getDate("date_from").toLocalDate();
                    LocalDate dateEnd = resultSet.getDate("date_to").toLocalDate();
                    // wywolanie konstruktora z parametrami
                    ProjectGen pro = new ProjectGen(name, state, dateStart, dateEnd);
                    obsDataProject.add(pro);
                }
                return obsDataProject;
            case "user":
                while (resultSet.next()) {
                    String name = resultSet.getString("login");
                    obsDataUser.add(name);
                }
                return obsDataUser;
            case "kamienie_milowe":
                while (resultSet.next()) {
                    String name = resultSet.getString("nazwa");
                    String odp = resultSet.getString("odpowiedzialnosc");
                    LocalDate start = resultSet.getDate("date_from").toLocalDate();
                    LocalDate end = resultSet.getDate("date_to").toLocalDate();
                    Short prog = (short) resultSet.getInt("postep");
                    MilestoneGen mg = new MilestoneGen(name, odp, start, end, prog);
                    obsDataMilestone.add(mg);
                }
                return obsDataMilestone;
            case "projekt-date":
                while (resultSet.next()) {
                    LocalDate startProject = resultSet.getDate("date_from").toLocalDate();
                    LocalDate dateEndProject = resultSet.getDate("date_to").toLocalDate();
                    obsDateProject.addAll(startProject, dateEndProject);
                }
                return obsDateProject;
            case "note":
                while (resultSet.next()) {
                    String note = resultSet.getString("notatka");
                    obsNote.add(note);
                }
                return obsNote;
            case "projekt-name":
                while (resultSet.next()) {
                    String title = resultSet.getString("nazwa");
                    obsTitle.add(title);
                }
                return obsTitle;
        }
        closeStatement();
        return null;
    }

    /**
     * Metoda zwracająca zadania zależne od zalogowanego użytkownika.
     *
     * @param zapytanie - zapytanie SQL
     * @return - zwraca ObservableList klasy generycznej zależnej od
     * zalogowanego użytkownika.
     */
    public ObservableList getPersonTask(String zapytanie) {
        createStatement();
        ObservableList<TaskGen> obsDataTask = FXCollections.observableArrayList();
        try {
            resultSet = statement.executeQuery(zapytanie);
            switch (permissions) {
                case "admin":
                    while (resultSet.next()) {
                        //zwraca wszystkie zadania dla admina i kierownika
                        String title = resultSet.getString("nazwa_zadania");
                        String whoDoit = resultSet.getString("kto_wykonuje");
                        String Implementation = resultSet.getString("wrealizacji");
                        String priorytet = resultSet.getString("priorytet");
                        String dataFrom = resultSet.getString("data_from");
                        String dataTo = resultSet.getString("data_to");
                        int procentTask = resultSet.getInt("procentowe_ukonczenie");
                        TaskGen taskG = new TaskGen(title, whoDoit, priorytet, Implementation, dataFrom, dataTo, procentTask);
                        obsDataTask.add(taskG);
                    }
                    break;
                case "kierownik":
                    while (resultSet.next()) {
                        //zwraca wszystkie zadania dla admina i kierownika
                        String title = resultSet.getString("nazwa_zadania");
                        String whoDoit = resultSet.getString("kto_wykonuje");
                        String Implementation = resultSet.getString("wrealizacji");
                        String priorytet = resultSet.getString("priorytet");
                        String dataFrom = resultSet.getString("data_from");
                        String dataTo = resultSet.getString("data_to");
                        int procentTask = resultSet.getInt("procentowe_ukonczenie");
                        TaskGen taskG = new TaskGen(title, whoDoit, priorytet, Implementation, dataFrom, dataTo, procentTask);
                        obsDataTask.add(taskG);
                    }
                    break;
                case "pracownik":
                    while (resultSet.next()) {
                        //zwraca wszystkie zadania dla admina i kierownika
                        String title = resultSet.getString("nazwa_zadania");
                        String whoDoit = resultSet.getString("kto_wykonuje");
                        String Implementation = resultSet.getString("wrealizacji");
                        String priorytet = resultSet.getString("priorytet");
                        String dataFrom = resultSet.getString("data_from");
                        String dataTo = resultSet.getString("data_to");
                        int procentTask = resultSet.getInt("procentowe_ukonczenie");
                        String[] whoDoThis = whoDoit.split(",");
                        for (int i = 0; i < whoDoThis.length; i++) {
                            String trim = whoDoThis[i].trim();
                            if (trim.equals(activeUser)) {
                                TaskGen taskG = new TaskGen(title, whoDoit, priorytet, Implementation, dataFrom, dataTo, procentTask);
                                obsDataTask.add(taskG);
                                break;
                            } else if (!trim.equals(activeUser)) {
                                continue;
                            } else {
                                break;
                            }
                        }
                    }
                    break;
            }
            return obsDataTask;
        } catch (SQLException ex) {
        }
        closeStatement();
        return null;
    }

    /**
     * Metoda pobiera nazwę użytkownika, ilość i procentowy postęp wykonania
     * zadania przez konkretnego pracownika.
     *
     * @param name - nazwa użytkownika
     * @param idProjekt - id
     * @return - zwraca ObservableList obliczone procentowo zadania.
     */
    public ObservableList<PieChart.Data> getChartData(String name,int idProjekt) {
        createStatement();
        ObservableList<PieChart.Data> result = FXCollections.observableArrayList();
        
        try {
            
            String query = "SELECT (select count(*) from zadania z,projekt p where kto_wykonuje LIKE '%" + name + "%' &&  p.id_projekt='"+idProjekt+"' && z.projekt_id_projekt='"+idProjekt+"') "
                    + " as iloscw, sum(procentowe_ukonczenie) as suma FROM zadania z,projekt p WHERE kto_wykonuje LIKE '%" + name + "%' &&  p.id_projekt='"+idProjekt+"' && z.projekt_id_projekt='"+idProjekt+"'";
            // preparedStatement  z argumentem zapytania query
            PreparedStatement preparedStmt = connection.prepareStatement(query);
            ResultSet re = preparedStmt.executeQuery();
            if (re.next()) {
                if (re.getInt("iloscw") == 0) {
                } else {
                    double wyk = (re.getInt("suma") / re.getInt("iloscw"));
                    double niewyk = 100 - (re.getInt("suma") / re.getInt("iloscw"));
                    result.add(new PieChart.Data(resourceBundle.getString("complete")+" " + wyk + " %", wyk));
                    result.add(new PieChart.Data(resourceBundle.getString("incomplete")+" " + niewyk + " %", niewyk));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        closeStatement();
        return result;
    }


    /**
     * Metoda zamykająca połączenie z bazą danych
     *
     */
    public void closeDB() {
        try {
            connection.close();
        } catch (SQLException ex) {
            pow.warningNotification("Problem z zamknięciem połączenie !");
        }
    }
}
