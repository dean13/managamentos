/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screenloading;

/**
 * Klasa generująca PDF rozszerzająca ładowanie w tle kalsy AsyncTaskFx
 *
 * @author Alan
 */
public class GeneratePDF extends AsyncTaskFx {

    /**
     * metoda wykonywana przed wykonaniem zadania np załadowanie LoadingScreena
     */
    @Override
    protected void onPreliminaryExecute() {
    }

    /**
     * metoda wykonywana podczas
     */
    @Override
    protected boolean doInBackground() {
        return false;
    }

    @Override
    protected void onPostExecute(boolean flag) {
    }

}
