/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screenloading;

import Baza.Connect;
import Controller.MainWindow;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Krzyskiek , Mateusz
 */
public class Test extends AsyncTaskFx {

    private LoadingScreen lS;
    final private Stage stage;
    final private Connect con;
    final private String login, password;
    final private Label tx;
    final private String id_user;
    final private String date;
    final private ResourceBundle resourceBundle;

    public Test(Stage stage, Connect con, String login, String password, Label tx, String userID, String data, ResourceBundle res) {
        this.stage = stage;
        this.con = con;
        this.login = login;
        this.password = password;
        this.tx = tx;
        this.date = data;
        this.id_user = userID;
        this.resourceBundle = res;
    }

    @Override
    protected void onPreliminaryExecute() {
        createPreloaderScene(stage, "Logowanie");
    }

    @Override
    protected boolean doInBackground() {

        Connect.connectserver();
        Connect.checkUser(login, password);

        if (login.equals(Connect.login) && password.equals(Connect.password));

        try {
            Connect.EasyInsert1("Insert into historia_logowania(user_id_osoby,data_logowania) values(" + id_user + ",'" + date + "')");
        } catch (Exception ex) {
        }

        return true;
    }

    @Override
    protected void onPostExecute(boolean flag) {
        if (flag) {

            //stage.close();
            lS.close();

            MainWindow mainWindow = new MainWindow(resourceBundle);
            mainWindow.show();

        } else {
            lS.close();
            tx.setText("Błędne dane !");
        }

    }

    private void createPreloaderScene(Stage stage, String title) {
        lS = new LoadingScreen(title);
        lS.initModality(Modality.WINDOW_MODAL);
        lS.initOwner(stage);
        lS.show();
    }
}
