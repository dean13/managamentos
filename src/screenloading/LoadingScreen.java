/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screenloading;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Klasa LoadinScreen - definiująca wygląd okienka logowania.
 *
 * @author Wszyscy
 */
public class LoadingScreen extends Stage {
    public LoadingScreen(final String title) {
        BorderPane root = new BorderPane();
        Text tx = new Text(title);
        tx.setStyle("-fx-fill:white;");
        HBox hbox = new HBox();
        hbox.setStyle("-fx-background-color: #3771AB;"
                + "-fx-padding: 15 12 15 12;"
                + "-fx-spacing: 10;"
                + "-fx-font-size: 20pt;");
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().add(tx);
        
        final ProgressIndicator pin = new ProgressIndicator();

        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(10));
        vbox.getChildren().addAll(pin);

        root.setTop(hbox);
        root.setCenter(vbox);
        root.setStyle("-fx-border-width: 2;-fx-border-color: #3771AB;");

        Scene scene = new Scene(root, 300, 150, Color.WHITE);
        //scene.getStylesheets().add("kolejkamanager/fxmlmainwindow.css");
        this.initStyle(StageStyle.TRANSPARENT);
        this.setScene(scene);

    }

}
