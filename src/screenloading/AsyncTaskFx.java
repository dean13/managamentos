/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screenloading;

import javafx.application.Platform;

/**
 *
 * @author Krzysiek, Mateusz 
 */
public abstract class AsyncTaskFx {

    private boolean daemon = true;

    /**
     * Zadania wykonywane na poczatku
     */
    protected abstract void onPreliminaryExecute();

    /**
     * Zadania wykonywane w trakcie
     *
     * @return - zwraca ?
     */
    protected abstract boolean doInBackground();

    /**
     * zadania wykonywane po background
     *
     * @param flag - ?
     */
    protected abstract void onPostExecute(final boolean flag);

    private final Thread bGthread = new Thread(() -> {
        boolean flag = doInBackground();

        Platform.runLater(() -> {
            onPostExecute(flag);
        });
    });

    /**
     * Wywołanie akcji
     */
    public void execute() {

        Platform.runLater(() -> {
            onPreliminaryExecute();

            bGthread.setDaemon(daemon);
            bGthread.start();
        });
    }

    public void setDaemon(boolean daemon) {

        this.daemon = daemon;
    }

    /**
     * Zatrzymanie akcji
     */
    public void interrupt() {
        this.bGthread.interrupt();
    }
}
