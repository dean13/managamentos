/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import java.io.IOException;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Pantera
 */
public class SettingsWindow extends Stage {

    public SettingsWindow(ResourceBundle resourceBundle) throws IOException {
        String title = resourceBundle.getString("titleSettings");
        this.setTitle(title);
        this.getIcons().add(new Image("zdjecia/Ikona.jpg"));

        FXMLLoader loader = new FXMLLoader();
        loader.setResources(resourceBundle);
        loader.setLocation(getClass().getResource("FXMLSettingsWindow.fxml"));
        Parent root = loader.load(getClass().getResource("FXMLSettingsWindow.fxml").openStream());
        FXMLSettingsWindowController controller = loader.getController();
        controller.setStage(SettingsWindow.this);
        controller.initialize(null, resourceBundle);
        Scene scene = new Scene(root);
        this.setScene(scene);
    }

}
