/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package settings;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import properties.SaveProperties;

/**
 * FXML Controller class
 *
 * @author Pantera
 */
public class FXMLSettingsWindowController implements Initializable {

    private Stage stage;
    @FXML
    private ComboBox comboLanguage;
    @FXML
    private Label info;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        switch (rb.getLocale().toLanguageTag()) {
            case "pl":
                comboLanguage.getSelectionModel().select(0);
                break;
            case "en":
                comboLanguage.getSelectionModel().select(1);
                break;
        }
        comboLanguage.valueProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                saveSystemLanguage(newValue);
                String str = rb.getString("infoRestart");
                info.setText(str);
            }
        });
    }

    private void saveSystemLanguage(String language) {
        Properties prop = new Properties();
        prop.setProperty("Language", language);
        SaveProperties sp = new SaveProperties();
        try {
            sp.zapisz(prop, "", SaveProperties.HOMESOURCE, "language");
        } catch (IOException ex) {
            Logger.getLogger(FXMLSettingsWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
