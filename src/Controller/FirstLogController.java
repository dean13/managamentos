/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Baza.Connect;
import Powiadomienia.Powiadomienia;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import properties.LoadProperties;
import properties.SaveProperties;

/**
 * FXML Controller class
 *
 * @author Kamil
 */
public class FirstLogController implements Initializable {
String tabela1="create table user\n" +
"(\n" +
"id_osoby INTEGER(10) AUTO_INCREMENT PRIMARY KEY,\n" +
"imie VARCHAR(30),\n" +
"nazwisko VARCHAR(30),\n" +
"login VARCHAR(30),\n" +
"haslo VARCHAR(30),\n" +
"email VARCHAR(40),\n" +
"uprawnienia VARCHAR(30),\n" +
"pracownik_ukonczone INTEGER(4)\n" +
");";

String tabela2="create table historia_logowania\n" +
"(\n" +
"user_id_osoby INTEGER(10),\n" +
"data_logowania DATETIME\n" +
");";
String tabela3="create table projekt\n" +
"(\n" +
"id_projekt INTEGER (10) AUTO_INCREMENT,\n" +
"nazwa VARCHAR(30),\n" +
"aktywnosc VARCHAR(30),\n" +
"date_from DATE,\n" +
"date_to DATE,\n" +
"PRIMARY KEY(id_projekt) \n" +
");";

String tabela4="create table zadania \n" +
"(\n" +
"id_zadania INTEGER(10) AUTO_INCREMENT,\n" +
"projekt_id_projekt INTEGER(10),\n" +
"nazwa_zadania VARCHAR(30),\n" +
"kto_wykonuje VARCHAR(150),\n" +
"wrealizacji VARCHAR(30),\n" +
"priorytet VARCHAR(30), \n" +
"data_from DATE, \n" +
"data_to DATE,\n" +
"procentowe_ukonczenie VARCHAR(4),\n" +
"notatka VARCHAR(300) NULL,\n" +
"PRIMARY KEY(id_zadania), \n" +
"FOREIGN KEY(projekt_id_projekt) REFERENCES projekt(id_projekt) ON DELETE CASCADE\n" +
");";

String tabela5="create table historia_realizacji \n" +
"(\n" +
"zadania_id_zadania INTEGER(10),\n" +
"user_id_osoby INTEGER(10),\n" +
"date_from DATE,\n" +
"date_to DATE,\n" +
"FOREIGN KEY(zadania_id_zadania) REFERENCES zadania(id_zadania) ON DELETE CASCADE,\n" +
"FOREIGN KEY(user_id_osoby) REFERENCES user(id_osoby) ON DELETE CASCADE\n" +
");";

String tabela6="create table historia_odpowiedzialnosci \n" +
"(\n" +
"zadania_id_zadania INTEGER(10),\n" +
"user_id_osoby INTEGER(10),\n" +
"date_from DATE,\n" +
"date_to DATE,\n" +
"FOREIGN KEY(zadania_id_zadania) REFERENCES zadania(id_zadania) ON DELETE CASCADE,\n" +
"FOREIGN KEY(user_id_osoby) REFERENCES user(id_osoby) ON DELETE CASCADE\n" +
");";

String tabela7="create table kamienie_milowe\n" +
"(\n" +
"id_kamienie_milowe INTEGER(10) AUTO_INCREMENT,\n" +
"nazwa VARCHAR(30),\n" +
"projekt_id_projekt INTEGER(10),\n" +
"date_from DATE,\n" +
"date_to DATE,\n" +
"odpowiedzialnosc VARCHAR(60),\n" +
"postep VARCHAR(4),\n" +
"PRIMARY KEY(id_kamienie_milowe),\n" +
"FOREIGN KEY(projekt_id_projekt) REFERENCES projekt(id_projekt) ON DELETE CASCADE\n" +
");";



    
    @FXML
    private TextField adres;
    @FXML
    private TextField nazwabazy; 
    @FXML
    private TextField login;
    @FXML
    private PasswordField haslo;
    @FXML
    private TextField nazwasys;
    @FXML
    private TextField haslosys;
    @FXML
    private Button buttoncreate;
  
    private Stage stage;
    private ResourceBundle resourceBundle;
    //kod do przetworzenia bazy
    public String kod;
    public String linia="";
    Connect con=new Connect();
    private Powiadomienia pow;
    
    //zmienna sciezki zapisu

   
    /**
     * Initializes the controller class.
     * @param stage - stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        resourceBundle = rb;
        pow = new Powiadomienia(rb);
    }    
    
    @FXML
    private void createdatabase(ActionEvent event) {
       
        
        //pobranie adresu bazy danych
        String adres1=adres.getText();
        //pobranie nazwy bazy
        String nazwabazy1=nazwabazy.getText();
        //pobranie loginu
        String login1=login.getText();
        //pobranie hasla
        String haslo1=haslo.getText();
        //pobranie nazwy systemu
        String namemanagementos1=nazwasys.getText();
        //haslo do systemu
        String passwordmana1=haslosys.getText();
        
        if(adres1.equals("") || nazwabazy1.equals("") || login1.equals("") || haslo1.equals("") || namemanagementos1.equals("") || passwordmana1.equals("") )
        {
            
          pow.informationNotification(resourceBundle.getString("fillFields"));
        }
        else
        {

        //zapisanie do trzech osobnych plikow
        createDocument(adres1, nazwabazy1, login1, haslo1);
        
       
       
        
        //tworzenie schematu bazy 
        createSchema(adres1,nazwabazy1,login1,haslo1,namemanagementos1,passwordmana1);
        
        // po poprawnym utworzeniu schematu zmieniamy flage
        changeSchema();
        
        stage.close();
        ResourceBundle rb = ResourceBundle.getBundle("bundles.bundle",getSystemLanguage());
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(rb);
        
        Parent root; 
        try{
        loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
        root = loader.load(getClass().getResource("FXMLDocument.fxml").openStream());
        FXMLDocumentController controller = loader.getController();
        controller.setStage(stage);
        controller.initialize(null, rb);

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Managementos");
        stage.show();
        }catch(Exception e)
        {
            
        }
        
        }
        
        
        
}
     
   
    /**
     * 
     * @param adres -adres bazy danych
     * @param nazwa - nazwa bazy danych
     * @param login - login bazy danych
     * @param haslo - haslo bazy danych
     * @param nameM - nazwa uzytkownika systemu
     * @param passwordM - haslo uzytkownika systemu
     */
    public void createSchema(String adres,String nazwa,String login,String haslo,String nameM,String passwordM)
    {
        
        //Tworzenie schematu bazy danych
        Connect.connectserverfirst(adres,nazwa,login,haslo);
        try {
            con.Update(tabela1);
            con.Update(tabela2);
            con.Update(tabela3);
            con.Update(tabela4);
            con.Update(tabela5);
            con.Update(tabela6);
            con.Update(tabela7);
            con.Update("Insert into user(login,haslo,uprawnienia) values('"+nameM+"','"+passwordM+"',"+"'admin')");
        } catch (SQLException ex) {
            Logger.getLogger(FirstLogController.class.getName()).log(Level.SEVERE, null, ex);
        }
        con.closeDB();
        pow.informationNotification(resourceBundle.getString("createStructureDatabase"));
        
        
    }
    /**
     * 
     * @return zwraca wybrany język 
     */
   Locale getSystemLanguage() {

        try {
            Properties prop = new LoadProperties().wczytaj(SaveProperties.HOMESOURCE + "language");
            String jezykZProperties = prop.getProperty("Language");
            return new Locale(jezykZProperties);
            

        }//try
        catch (IOException ex) {
            // Logger.getLogger(JavaFXApplication1.class.getName()).log(Level.WARNING, null, ex);

            return new Locale(System.getProperty("user.language"));
        }

    } 
    
    /**
     * 
     * @param adres - zapis do pliku adresu bazy
     * @param nazwa - zapis do pliku nazwy bazy
     * @param login - zapis do pliku loginu bazy
     * @param haslo - zapis do pliku hasla bazy
     */
    public void createDocument(String adres,String nazwa,String login,String haslo ){

            try{
           
            //nazwa pliku
            String sciezka=properties.SaveProperties.HOMESOURCE+"danedostepowe";
            //tworzenie i zapis do pliku adress
            File plik = new File(sciezka);
            //tworzenie struktury folderow
            if (!plik.exists()) {
            plik.mkdirs();
            }
            
            //zapis pliku
            sciezka=properties.SaveProperties.HOMESOURCE+"danedostepowe\\adres.txt";
            //tworzenie i zapis do pliku adress
            plik = new File(sciezka);
            PrintWriter zapis = new PrintWriter(plik);
            zapis.write(adres);
            zapis.close();
            
          
            } catch(Exception e)
            {
                System.out.println("wyjatek"+e);
            }
            //------------------------zapis do nazwabazy
            try{
            //nazwa pliku
             String sciezka=properties.SaveProperties.HOMESOURCE+"danedostepowe\\nazwabazy.txt";
            File plik = new File(sciezka);
            PrintWriter zapis = new PrintWriter(plik);
            zapis.write(nazwa);
            zapis.close();
            } catch(Exception e)
            {
                System.out.println("wyjatek"+e);
            }
            
            //------------------------zapis do login
             try{
            //nazwa pliku
            String sciezka=properties.SaveProperties.HOMESOURCE+"danedostepowe\\login.txt";
            //tworzenie i zapis do pliku adress
            File plik = new File(sciezka);
            PrintWriter zapis = new PrintWriter(plik);
            zapis.write(login);
            zapis.close();
            } catch(Exception e)
            {
                System.out.println("wyjatek"+e);
            }
             
            //------------------zapis haslo 
             
              try{
            //nazwa pliku
            String sciezka=properties.SaveProperties.HOMESOURCE+"danedostepowe\\haslo.txt";
            //tworzenie i zapis do pliku adress
            File plik = new File(sciezka);
            PrintWriter zapis = new PrintWriter(plik);
            zapis.write(haslo);
            zapis.close();
            } catch(Exception e)
            {
                System.out.println("wyjatek"+e);
            }
            
              
           
            
        
        }
    
     /**
      * Metdoa changeSchema zmieniająca flage pierwszego poprawnego logowania na true
      */
    public void changeSchema()
    {
        try{
            //nazwa pliku
            String sciezka=properties.SaveProperties.HOMESOURCE+"danedostepowe\\flaga.txt";
            //tworzenie i zapis do pliku adress
            File plik = new File(sciezka);
            PrintWriter zapis = new PrintWriter(plik);
            zapis.write("1");
            zapis.close();
            } catch(Exception e)
            {
                System.out.println("wyjatek"+e);
            }
    }
    
    
    public boolean checDatabase(){
        
        
        return true;
    }

  
}

