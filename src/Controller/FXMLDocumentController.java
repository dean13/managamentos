/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Baza.Connect;
import Generic.ActiveUser;
import Powiadomienia.Powiadomienia;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import screenloading.Test;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.scene.control.Hyperlink;

/**
 * Kontroler do FXMLMain.fxml okno logowania
 *
 * @author Kamil W, Kamil Ż, Alan U
 */
public class FXMLDocumentController implements Initializable {

    private ResourceBundle resourceBundle;
    public Connect con;
    private Powiadomienia pow;
    private Stage stage;
    @FXML
    public Label lInfo;
    @FXML
    public TextField txLogin;
    @FXML
    public PasswordField tfPassword;
    @FXML
    private Button bZaloguj;
    @FXML
    private Hyperlink rejestracja;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Metoda do anulowania logowania do aplikacji
     *
     * @param event - zdarzenie akcji naciśniecia przycisku
     */
    @FXML
    private void handleAnuluj(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    private void signIn(ActionEvent event) {
        System.out.println("Zarejestrowano!!!!");
    }

    /**
     * Metoda do zalogowania do programu.
     *
     * @param event - zdarzenie akcji naciśniecia przycisku
     * @see Controller.FXMLDocumentController#loginToBase()
     */
    @FXML
    private void handleZaloguj(ActionEvent event) {
        loginToBase();
    }

    /**
     * Metoda nasłuchująca naciśnięcie klawisza Enter w oknie logowania
     *
     * @param url - domyślna wartość
     * @param rb - domyślna wartość
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txLogin.setOnKeyPressed((KeyEvent key) -> {
            if (key.getCode() == KeyCode.ENTER) {
                loginToBase();
            }
        });

        tfPassword.setOnKeyPressed((KeyEvent key) -> {
            if (key.getCode() == KeyCode.ENTER) {
                loginToBase();
            }
        });
        resourceBundle = rb;
        con = new Connect(resourceBundle);
        pow = new Powiadomienia(rb);
    }

    /**
     * Metoda sprawdzająca czy wprowadziliśmy poprawne dane logowania i
     * rejestrująca użytkownika do tabeli Historia logowania.
     */
    public void loginToBase() {    
        if (txLogin.getText().isEmpty() || tfPassword.getText().isEmpty()) {
            String info = resourceBundle.getString("warningLabel");
            pow.warningNotification(info);
        } else {
            if (Connect.connectserver() == true) {
                Connect.checkUser(txLogin.getText(), tfPassword.getText());
                if (txLogin.getText().equals(Connect.login) && tfPassword.getText().equals(Connect.password)) {
                    // akutalna sesja uzytkownika
                    ActiveUser.setLogin(txLogin.getText());
                    String uprawnienia = Connect.uprawnnienia;
                    ActiveUser.setUprawnienia(uprawnienia);
                    String id_user = Connect.id_osoby;
                    ActiveUser.setIdUser(id_user);
                    //Dodanie do tabeli historia logowania aktualnie logowanego uzytkownika
                    SimpleDateFormat simpleDateHere = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    //System.out.println( simpleDateHere.format(new Date()) );
                    String date = simpleDateHere.format(new Date());
                    try {
                        //Connect.EasyInsert1("Insert into historia_logowania(user_id_osoby,data_logowania) values(" + id_user + ",'" + date + "')");
                        Test test = new Test(stage, con, txLogin.getText(), tfPassword.getText(), lInfo, id_user, date,resourceBundle);
                        test.execute();
                    } catch (Exception ex) {
                        String info = resourceBundle.getString("warningLabel1");
                        pow.warningNotification(info);
                    }
                    /*Test test = new Test(stage, con, txLogin.getText(), tfPassword.getText(), lInfo, id_user, date,resourceBundle);
                    test.execute();*/
                    stage.close();
                } else {
                    //String info = resourceBundle.getString("warningLabel2");
                    //pow.warningNotification(info);
                }
            }
        }
    }

}
