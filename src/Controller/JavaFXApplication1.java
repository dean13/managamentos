/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Baza.Connect;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import properties.LoadProperties;
import properties.SaveProperties;

/**
 *
 * @author student
 */
public class JavaFXApplication1 extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Image icon = new Image("zdjecia/Ikona.jpg");
        stage.getIcons().add(icon);
        ResourceBundle rb = ResourceBundle.getBundle("bundles.bundle", getSystemLanguage());
        FXMLLoader loader = new FXMLLoader();
        loader.setResources(rb);
        
        Parent root ;
        
        String flag;
         try{
          
           
        //nazwa pliku
        String sciezka=properties.SaveProperties.HOMESOURCE+"danedostepowe\\flaga.txt";
        //tworzenie i zapis do pliku adress
        File plik = new File(sciezka);
        //tworzenie struktury folderow
        if (plik.exists()) {
                

        loader.setLocation(getClass().getResource("FXMLDocument.fxml"));
        root = loader.load(getClass().getResource("FXMLDocument.fxml").openStream());
        FXMLDocumentController controller = loader.getController();
        controller.setStage(stage);
        controller.initialize(null, rb);

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Managementos");
        stage.show();
        
        }else
        {
        loader.setLocation(getClass().getResource("FirstLog.fxml"));
        root = loader.load(getClass().getResource("FirstLog.fxml").openStream());
        FirstLogController controller = loader.getController();
        controller.setStage(stage);
        controller.initialize(null, rb);

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Managementos");
        stage.show();
        
        
        
        }
            } catch(Exception e)
            {
                System.out.println("wyjatek"+e);
            }
     
       
    }

    Locale getSystemLanguage() {

        try {
            Properties prop = new LoadProperties().wczytaj(SaveProperties.HOMESOURCE + "language");
            String jezykZProperties = prop.getProperty("Language");
            return new Locale(jezykZProperties);

        }//try
        catch (IOException ex) {
            // Logger.getLogger(JavaFXApplication1.class.getName()).log(Level.WARNING, null, ex);

            return new Locale(System.getProperty("user.language"));
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
