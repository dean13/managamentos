/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Baza.Connect;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.collections.ObservableList;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.util.Callback;

/**
 *
 * @author Alan U
 */
public class DatePickerLock {

    private final DatePicker datePickerStart;
    private final DatePicker datePickerEnd;

    public DatePickerLock(DatePicker startDate, DatePicker endDate) {
        this.datePickerStart = startDate;
        this.datePickerEnd = endDate;
    }

    //metoda ustawia datepickera dla Zakładki projekty
    public void setCellFactory() {
        final Callback<DatePicker, DateCell> dayCellFactory
                = new Callback<DatePicker, DateCell>() {
                    @Override
                    public DateCell call(final DatePicker datePicker) {
                        return new DateCell() {
                            @Override
                            public void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item.isBefore(LocalDate.now())) {
                                    this.setDisable(true);
                                    setStyle("-fx-background-color: #ffc0cb;");
                                }
                            }
                        };
                    }
                };
        datePickerStart.setValue(LocalDate.now());
        datePickerStart.setDayCellFactory(dayCellFactory);
        datePickerEnd.setValue(LocalDate.now());
        datePickerEnd.setDayCellFactory(dayCellFactory);
    }

    //Metoda zwraca tablicę zablokowany DatePicker dla daty rozpoczynającej się i kończącej
    public void setCellFactoryTaskAndMilestone(int idProjekt) {
        Connect con = new Connect();
        //final ObservableList<LocalDate> obsDateProject = FXCollections.observableArrayList();
        try {
            final ObservableList<LocalDate> obsDateProject = con.selectTable("SELECT date_from, date_to FROM projekt WHERE id_projekt='" + idProjekt + "'", "projekt-date");
            
            final Callback<DatePicker, DateCell> dayCellFactoryStart
                    = new Callback<DatePicker, DateCell>() {
                        @Override
                        public DateCell call(final DatePicker datePicker) {
                            return new DateCell() {
                                @Override
                                public void updateItem(LocalDate item, boolean empty) {
                                    super.updateItem(item, empty);
                                    if (item.isBefore(obsDateProject.get(0))) {
                                        this.setDisable(true);
                                        setStyle("-fx-background-color: #ffc0cb;");
                                    }
                                }
                            };
                        }
                    };
            datePickerStart.setValue(obsDateProject.get(0));
            datePickerStart.setDayCellFactory(dayCellFactoryStart);

            final Callback<DatePicker, DateCell> dayCellFactoryEnd
                    = new Callback<DatePicker, DateCell>() {
                        @Override
                        public DateCell call(final DatePicker datePicker) {
                            return new DateCell() {
                                @Override
                                public void updateItem(LocalDate item, boolean empty) {
                                    super.updateItem(item, empty);
                                    if (item.isAfter(obsDateProject.get(1))) {
                                        this.setDisable(true);
                                        setStyle("-fx-background-color: #ffc0cb;");
                                    }
                                    else if (item.isBefore(obsDateProject.get(0))) {
                                        this.setDisable(true);
                                        setStyle("-fx-background-color: #ffc0cb;");
                                    }
                                }
                            };
                        }
                    };
            datePickerEnd.setValue(LocalDate.now());
            datePickerEnd.setDayCellFactory(dayCellFactoryEnd);

        } catch (SQLException ex) {
        }

    }
}
