/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Baza.Connect;
import Generic.ActiveUser;
import Generic.MilestoneGen;
import Generic.NewUser;
import Generic.ProjectGen;
import Generic.TablePerson;
import Generic.TaskGen;
import MainWindow.Milestone;
import MainWindow.Person;
import MainWindow.Project;
import MainWindow.Raports;
import MainWindow.Task;
import Powiadomienia.Powiadomienia;
import Uprawnienia.Permissions;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import screenloading.LoadingScreen;
import settings.SettingsWindow;

/**
 * Kontroller dla głównego okna aplikacji
 *
 * @author - ekipa Managementos
 */
public class FXMLMainController implements Initializable {

    /**
     * Prywatna instancja klasy stage
     */
    private Stage stage;
    /**
     * Instancja klasy powiadomienia
     */
    public Powiadomienia pow;
    /**
     * Instancja klasy projekt
     */
    public Project project = new Project();
    /**
     * Instancja klasy task
     */
    public Task taskTab;
    /**
     * Instancja klasy projectGen
     */
    ProjectGen proj;
    /**
     * Instancja klasy taskGen
     */
    TaskGen task;
    private LoadingScreen lS;

    /**
     * Metoda ustawiająca stage
     *
     * @param stage - obiekt klasy stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private BorderPane rootWykres;

    //pola do zakładki Projekt
    /**
     * tfNameProject - pole tekstowe nazwa projektu
     */
    @FXML
    public TextField tfNameProject;
    /**
     * dpStartDateProject - DatePicker do wybrania rozpoczęcia projektu
     */
    @FXML
    public DatePicker dpStartDateProject;
    /**
     * dpEndDateProject - DatePicker do wybrania zakończenia projektu
     */
    @FXML
    public DatePicker dpEndDateProject;
    /**
     * chState - ComboBox ze statusem projektu
     */
    @FXML
    public ComboBox chState;
    /**
     * bRemove - przycisk Usuń projekt
     */
    @FXML
    public Button bRemove;
    /**
     * bEdit - przycisk edytuj projekt
     */
    @FXML
    public Button bEdit;
    /**
     * bSave - przycisk zapisz projekt
     */
    @FXML
    public Button bSave;

    @FXML
    public Tab osobyTab;

    //pola do zakładki raporty
    @FXML
    public ChoiceBox chChooseProjRap;
    @FXML
    private Button bGenerujRap;
    @FXML
    public Tab raportyTab;

    //Pola do zakładki Kamienie Milowe
    @FXML
    private TextField tfNazwaKamienia;
    @FXML
    private DatePicker dpStartKM;
    @FXML
    private DatePicker dpKoniecKM;
    @FXML
    private ChoiceBox cbProjectKM;
    @FXML
    private Button bDodajKM;
    @FXML
    private Button bUsunKM;
    @FXML
    public ListView<String> lvOdpowiedzialni = new ListView<>();
    @FXML
    private TableView<MilestoneGen> tableKM;
    @FXML
    private TableColumn<MilestoneGen, String> colNazwa;
    @FXML
    private TableColumn<MilestoneGen, String> colProjekt;
    @FXML
    private TableColumn<MilestoneGen, String> colStart;
    @FXML
    private TableColumn<MilestoneGen, String> colEnd;
    @FXML
    private TableColumn<MilestoneGen, String> colOdpowiedzialni;
    @FXML
    private TableColumn<MilestoneGen, Double> colPostep;

    String progressMilestone;
    String selectedMilestone = null;
    public Milestone milestone;
    ObservableList<String> projects;

    private ObservableList<MilestoneGen> tableMilestoneData = FXCollections.observableArrayList();
    //private ObservableList<String> projects = FXCollections.observableArrayList();
    private ObservableList<LocalDate> obsDateProject = FXCollections.observableArrayList();

    //pola do zakładki osoby
    @FXML
    public Button bEditPerson;
    @FXML
    public Button bAddPerson;
    @FXML
    public Button bRemovePerson;
    @FXML
    public TextField namePerson;
    @FXML
    public TextField surnamePerson;
    @FXML
    public TextField loginPerson;
    @FXML
    public TextField passwordPerson;
    @FXML
    public TextField emailPerson;
    @FXML
    public ChoiceBox chChoiceFunction;

    //pola do zakładki zadania
    /**
     * bAddTask - przycisk dodaj zadanie
     */
    @FXML
    public Button bAddTask;
    /**
     * tfTaskName - pole tekstowe nazwa zadania
     */
    @FXML
    public TextField tfTaskName;
    /**
     * taTaskNote - pole tekstowe notatka do zadania
     */
    @FXML
    public TextArea taTaskNote;
    /**
     * lvUsers - lista użytkowników jako ComboBox
     */
    @FXML
    public ListView<String> lvUsers = new ListView<>();
    /**
     * dpTaskSince - DatePicker wyboru początku terminu zadania
     */
    @FXML
    public DatePicker dpTaskSince;
    /**
     * dpTaskTo - DatePicker wyboru końca terminu zadania
     */
    @FXML
    public DatePicker dpTaskTo;
    /**
     * chbProjects - ChoiceBox z wyborem dostępnych projektów
     */
    @FXML
    public ChoiceBox chbProjects;
    /**
     * chbImplementation - ChoiceBox z wybraniem statusu zadania
     */
    @FXML
    public CheckBox chbImplementation;
    /**
     * cbPriorytet - ComboBox z wyborem priorytetu zadania
     */
    @FXML
    public ComboBox cbPriorytet;
    /**
     * idProjekt - przechowuje idprojektu
     */
    public int idProjekt;
    /**
     * nameUser - ArrayList przechowująca nazwy użytkowników
     */
    public ArrayList<String> nameUser = new ArrayList<>();
    /**
     * map - mapa (idprojektu, nazwa_projektu)
     */
    public Map<String, String> map = new HashMap<>();
    /**
     * progressTask - procentowe ukończenie zadania
     */
    /**
     * pressedTask - nazwa klikniętego zadania
     */
    public String progressTask, pressedTask;

    //tabela Projektu
    /**
     * tableProject - widok tabeli Projekt
     */
    @FXML
    private TableView<ProjectGen> tableProject;
    /**
     * columnNameProject - zawartość kolumny Nazwa
     */
    @FXML
    private TableColumn<ProjectGen, String> columnNameProject;
    /**
     * columnDateStartProject - zawartość kolumny Data rozpoczęcia
     */
    @FXML
    private TableColumn<ProjectGen, String> columnDateStartProject;
    /**
     * columnDateEndProject - zawartość kolumny Data zakończenia
     */
    @FXML
    private TableColumn<ProjectGen, String> columnDateEndProject;
    /**
     * columnStateProject - zawartość kolumny Status projektu
     */
    @FXML
    private TableColumn<ProjectGen, String> columnStateProject;

    //tabela Zadania
    /**
     * tableTask - widok tabeli Zadania
     */
    @FXML
    public TableView<TaskGen> tableTask;
    /**
     * columnTitle - zawartość kolumny Tytuł
     */
    @FXML
    private TableColumn<TaskGen, String> columnTitle;
    /**
     * columnInvolving - zawartość kolumny W realizacji
     */
    @FXML
    private TableColumn<TaskGen, String> columnInvolving;
    /**
     * columnImplementation - zawartość kolumny Z udziałem
     */
    @FXML
    private TableColumn<TaskGen, String> columnImplementation;
    /**
     * columnPriorytet - zawartość kolumny Priorytet
     */
    @FXML
    private TableColumn<TaskGen, String> columnPriorytet;
    /**
     * columnDateStartTask - zawartość kolumny Czas od
     */
    @FXML
    private TableColumn<TaskGen, String> columnDateStartTask;
    /**
     * columnDateEndTask - zawartość kolumny Czas do
     */
    @FXML
    private TableColumn<TaskGen, String> columnDateEndTask;
    @FXML
    private TableColumn<TaskGen, Double> columnProcentTask;

    //tabela Person - zawiera historie logowania
    @FXML
    private TableView<TablePerson> tablePerson;
    @FXML
    private TableColumn<TablePerson, String> loginPersonTable;
    @FXML
    private TableColumn<TablePerson, String> surnamePersonTable;
    @FXML
    private TableColumn<TablePerson, String> loggedPersonTable;

    //tabela uzytkownicy
    @FXML
    private TableView<NewUser> tablePerson1;
    @FXML
    private TableColumn<NewUser, String> nameUser1;
    @FXML
    private TableColumn<NewUser, String> surname1;
    @FXML
    private TableColumn<NewUser, String> login1;
    @FXML
    private TableColumn<NewUser, String> email1;
    @FXML
    private TableColumn<NewUser, String> funkction;

    @FXML
    private Parent wykresView;

    //ikony do zakladek
    @FXML
    public Tab iconProjekty;
    @FXML
    public Tab iconMilestone;
    @FXML
    public Tab iconWykres;
    @FXML
    public Tab iconTask;

    /**
     * tablePersonData1 - dane dla tabeli użytkownicy
     */
    private ObservableList<NewUser> tablePersonData1 = FXCollections.observableArrayList();

    private ObservableList<TablePerson> tablePersonData = FXCollections.observableArrayList();
    /**
     * tableProjectData - dane dla tabeli Projekty
     */
    private ObservableList<ProjectGen> tableProjectData = FXCollections.observableArrayList();
    /**
     * tableTaskData - dane dla tabeli Zadania
     */
    public ObservableList<TaskGen> tableTaskData = FXCollections.observableArrayList();
    /**
     * stateCombo - przechowuje rodzaje statusu dla Projktu
     */
    public ObservableList<String> stateCombo = FXCollections.observableArrayList("Aktywny", "Wstrzymany", "Nie Aktywny", "Porzucony");
    /**
     * priorytetCombo - przechowuje rodzaje priorytetu dla Projktu
     */
    public ObservableList<String> priorytetCombo = FXCollections.observableArrayList("Mamy czas", "Średni", "Bardzo Pilny", "Na Wczoraj !");
    /**
     * listUsers - lista użytkowników
     */
    public ObservableList<String> listUsers = FXCollections.observableArrayList();
    /**
     * chChoiceFunction1 - funkcje użytkowników
     */
    public ObservableList<String> chChoiceFunction1 = FXCollections.observableArrayList("admin", "kierownik", "pracownik");

    /**
     * name - pomocnicza zmienna nazwa Projektu
     */
    /**
     * state - pomocnicza zmienna status Projektu
     */
    /**
     * nameOldProject - nazwa Projektu przed edycją
     */
    /**
     * titleTaskNew - nazwa Zadania po edycji
     */
    /**
     * titleTaskOld - nazwa Zadania przed edycją
     */
    public String name, state, nameOldProject, titleTaskNew, titleTaskOld;
    /**
     * dataSince - data rozpoczęcia zaznaczonego Projektu
     */
    /**
     * dataTo - data zakończenia zaznaczonego Projektu
     */
    public LocalDate dataSince, dataTo;
    public ResourceBundle resourceBundle;

    @FXML
    private void settingsWindow(ActionEvent event) {
        try {
            SettingsWindow settingsWindow = new SettingsWindow(resourceBundle);
            settingsWindow.initModality(Modality.WINDOW_MODAL);
            settingsWindow.initOwner(stage);
            settingsWindow.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metoda zapisująca nowy Projekt
     *
     * @param event - zdarzenie kliknięcia na przycisk Zapisz
     * @see Controller.FXMLMainController#reloadDatabase()
     * @see ProjectGen#ProjectGen(java.lang.String, java.lang.String,
     * java.time.LocalDate, java.time.LocalDate)
     * @see Project#saveProject(Generic.ProjectGen)
     */
    @FXML
    public void saveProject(ActionEvent event) {
        String nameProject = tfNameProject.getText();
        LocalDate startDate = dpStartDateProject.getValue();
        LocalDate endDate = dpEndDateProject.getValue();
        String stateProject = chState.getSelectionModel().getSelectedItem().toString();
        proj = new ProjectGen(nameProject, stateProject, startDate, endDate, resourceBundle);
        if (project.validateProject(proj) == true) {
            project.saveProject(proj);
            //();
            reloadDatabase();
            reloadProjectItem();
        }
    }

    /**
     * Metoda usuwająca Projekt
     *
     * @param event - zdarzenie kliknięcia na przycisk Usuń
     * @throws SQLException - przekazanie wyjątku klasę wyżej
     * @see Controller.FXMLMainController#reloadDatabase()
     * @see Project#delateProject(java.lang.String)
     */
    @FXML
    public void delateProject(ActionEvent event) throws SQLException {
        if (pow.confirmDeleteRecord() == true) {
            project.delateProject(nameOldProject);
            //clearObservableList();
            reloadDatabase();

            bAddTask.setDisable(true);
            bDodajKM.setDisable(true);
            bUsunKM.setDisable(true);
            tfNameProject.setText("");
            chState.getSelectionModel().select("Aktywny");
            dpStartDateProject.setValue(null);
            dpEndDateProject.setValue(null);
        }
    }

    /**
     * Metoda edytująca Projekt
     *
     * @param event - zdarzenie kliknięcia na przycisk Aktualizuj
     * @see Generic.ProjectGen#ProjectGen(java.lang.String, java.lang.String,
     * java.time.LocalDate, java.time.LocalDate)
     * @see Project#editProject(java.lang.String, java.lang.String,
     * java.lang.String, java.time.LocalDate, java.time.LocalDate)
     * @see #reloadDatabase()
     */
    @FXML
    public void editProject(ActionEvent event) {
        proj = new ProjectGen(tfNameProject.getText(),
                chState.getSelectionModel().getSelectedItem().toString(),
                dpStartDateProject.getValue(),
                dpEndDateProject.getValue());

        if (tfNameProject.getText().equals(nameOldProject)
                && chState.getSelectionModel().getSelectedItem().toString().equals(state)
                && dpStartDateProject.getValue().equals(dataSince) && dpEndDateProject.getValue().equals(dataTo)) {

        } else {
            project.editProject(nameOldProject, proj.getName(), proj.getStatus(), proj.getDateStart(), proj.getDateEnd(), resourceBundle);
            //clearObservableList();

            reloadDatabase();
        }

    }

    /**
     * Metoda zapisująca nowe zadanie do wybranego Projektu
     *
     * @param event - zdarzenie kliknięcia na przycisk Dodaj
     * @see TaskGen#TaskGen(java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String,
     * java.time.LocalDate, java.time.LocalDate)
     * @see Task#validateTask(Generic.TaskGen,
     * javafx.collections.ObservableList)
     * @see Task#addTask(Generic.TaskGen, int)
     * @see #reloadDatabase()
     */
    @FXML
    public void addTask(ActionEvent event) {
        String title = tfTaskName.getText();
        String note = taTaskNote.getText();
        LocalDate startDate = dpTaskSince.getValue();
        LocalDate endDate = dpTaskTo.getValue();
        String priorytet = cbPriorytet.getSelectionModel().getSelectedItem().toString();
        boolean onRealization = chbImplementation.isSelected();

        String realization;
        if (onRealization == true) {
            realization = "TAK";
        } else {
            realization = "NIE";
        }

        String users = "";
        for (String user : nameUser) {
            if (user.equals("")); else {
                users = users + user + ", ";
            }
        }
        task = new TaskGen(title, users, priorytet, note, realization, startDate, endDate);
        taskTab = new Task(resourceBundle);
        boolean validate = taskTab.validateTask(task, tableTaskData, idProjekt);
        if (validate == true) {
            taskTab.addTask(task, idProjekt);
            //clearObservableList();
            reloadDatabase();
        }
    }

    /**
     * Metoda initializująca zakładki (zakładka Projekty, Zadania, Osoby),
     * przeładowanie tabel i wykresów
     *
     * @param location - domyślna wartość
     * @param resources - domyślna wartość
     * @see #initializeTablePerson()
     * @see #initializeTablePerson1()
     * @see #initializeTableProjekt()
     * @see #initializeTableTask()
     * @see #reloadDatabase()
     * @see #initWykres()
     * @see #initReports()
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
        reloadDatabase();
        reload();
        /*initializeTableProjekt();
         initializeTableTask();
         initializeTablePerson();
         initializeTablePerson1();
         initializeMilestone();
         initReports();
         initWykres();*/
        pow = new Powiadomienia(resources);
    }

    /**
     * Przeładowanie wszystkich zakładek
     */
    public void reload() {
        reloadDatabase();
        initializeTableProjekt();
        initializeTableTask();
        initializeTablePerson();
        initializeTablePerson1();
        initializeMilestone();
        initReports();
        initWykres();
    }

    /**
     * Metoda do wykrywania kliknięcia na wiersz w tabeli Projekt
     *
     * @param event - zdarzenie kliknięcia
     * @exception NullPointerException - w razie klikięcia na pustą tabelę
     */
    @FXML

    public void clickedTable(MouseEvent event) {
        try {
            nameOldProject = tableProject.getSelectionModel().getSelectedItem().getName();
            state = tableProject.getSelectionModel().getSelectedItem().getStatus();
            dataSince = tableProject.getSelectionModel().getSelectedItem().getDateStart();
            dataTo = tableProject.getSelectionModel().getSelectedItem().getDateEnd();

            tfNameProject.setText(nameOldProject);
            chState.getSelectionModel().select(state);
            dpStartDateProject.setValue(dataSince);
            dpEndDateProject.setValue(dataTo);

            bRemove.setDisable(false);
            bEdit.setDisable(false);
            //bSave.setDisable(true);
        } catch (NullPointerException ex) {

        }
    }

    /**
     * Metoda oczekująca na kliknięcie w tabeli zadania
     *
     * @param event - zdarzenie kliknięcia
     */
    @FXML
    public void clickedTableTask(MouseEvent event) {
        try {
            pressedTask = tableTask.getSelectionModel().getSelectedItem().getTitle();
        } catch (NullPointerException eeeee) {

        }
    }

    @FXML
    public void overTable(MouseEvent event) {

    }

    /**
     * Listener do textFielda tylko Litery
     *
     * @param event - zdarzenie wciśnięcia klawisza
     */
    @FXML
    public void validateFieldLetters(KeyEvent event) {
        char[] ch = event.getCharacter().toCharArray();
        for (char d : ch) {
            if (Character.isLetter(d)); else {
                event.consume();
            }
        }

    }

    /**
     * Listener do textFielda Litery i cyfry
     *
     * @param event - zdarzenie wciśnięcia klawisza
     */
    @FXML
    public void validateFieldLettersDigit(KeyEvent event) {
        char[] ch = event.getCharacter().toCharArray();
        for (char d : ch) {
            if (Character.isLetter(d) || Character.isDigit(d)); else {
                event.consume();
            }
        }

    }

    /**
     * Dodanie pól z zakładki Osoby do klasy generycznej
     *
     * @see NewUser#NewUser(java.lang.String, java.lang.String,
     * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     * @see Person#Add(Generic.NewUser)
     */
    @FXML
    public void addPerson() {
        Connect connect = new Connect();
        String nameP = namePerson.getText();
        String surname = surnamePerson.getText();
        String login = loginPerson.getText();
        String password = passwordPerson.getText();
        String email = emailPerson.getText();
        String funkction = chChoiceFunction.getSelectionModel().getSelectedItem().toString();

        String confirmlogin = connect.Select("Select login from user where login=" + "'" + login + "'");

        if (nameP.equals("") || surname.equals("") || login.equals("") || password.equals("") || email.equals("")) {
            pow.warningNotification(resourceBundle.getString("fillFields"));
        } else {

            if (login.equals(confirmlogin)) {
                pow.warningNotification(resourceBundle.getString("problemCreateUser") + " " + login + ". " + resourceBundle.getString("problemCreateUserCd"));
            } else if (!email.contains("@")) {
                pow.warningNotification(resourceBundle.getString("correctMail"));
            } else {

                NewUser newUser = new NewUser(nameP, surname, login, password, email, funkction);
                Person newPerson = new Person(resourceBundle);
                newPerson.Add(newUser);
                reloadTablePerson1();
                initializeMilestone();
                initializeTableProjekt();
                initializeTableTask();

            }
        }
    }

    /**
     * Initializowanie zakładki Osoby
     *
     * @see Permissions#selectModel()
     * @see #reloadTablePerson()
     */
    public void initializeTablePerson() {
        Image image = new Image("zdjecia/icon_person.png");
        ImageView iv = new ImageView(image);

        osobyTab.setGraphic(iv);

        //model dla tabeli Projekty
        loginPersonTable.setCellValueFactory(new PropertyValueFactory<>("login"));
        surnamePersonTable.setCellValueFactory(new PropertyValueFactory<>("nazwisko"));
        loggedPersonTable.setCellValueFactory(new PropertyValueFactory<>("data"));

        chChoiceFunction.setItems(chChoiceFunction1);
        chChoiceFunction.setValue("admin");

        Permissions permision = new Permissions(osobyTab, raportyTab, bSave, bAddTask, dpTaskSince, dpTaskTo, dpStartDateProject,
                dpEndDateProject, tfTaskName, taTaskNote, lvUsers, chbProjects, chbImplementation, cbPriorytet, tfNameProject, chState, bRemove, bEdit, tableProject);
        permision.selectModel();
        reloadTablePerson();
    }

    /**
     * Przeładowanie zawartości tabeli Osoby
     *
     * @see Connect#SelectTablePerson(java.lang.String)
     */
    public void reloadTablePerson() {
        Connect con = new Connect();
        try {
            tablePersonData = con.SelectTablePerson("Select login,nazwisko,data_logowania from user as a join historia_logowania as b where a.id_osoby=b.user_id_osoby \n"
                    + "order by data_logowania desc");
            tablePerson.setItems(tablePersonData);

        } catch (SQLException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initializeTablePerson1() {
        //model dla tabeli Projekty
        nameUser1.setCellValueFactory(new PropertyValueFactory<>("name"));
        surname1.setCellValueFactory(new PropertyValueFactory<>("surname"));
        login1.setCellValueFactory(new PropertyValueFactory<>("login"));
        email1.setCellValueFactory(new PropertyValueFactory<>("email"));
        funkction.setCellValueFactory(new PropertyValueFactory<>("funkction"));

        Permissions permision = new Permissions(osobyTab, raportyTab, bSave, bAddTask, dpTaskSince, dpTaskTo, dpStartDateProject,
                dpEndDateProject, tfTaskName, taTaskNote, lvUsers, chbProjects, chbImplementation, cbPriorytet, tfNameProject, chState, bRemove, bEdit, tableProject);
        permision.selectModel();
        reloadTablePerson1();
    }

    public void reloadTablePerson1() {
        Connect con = new Connect();
        try {
            tablePersonData1 = con.SelectTablePerson1("Select imie,nazwisko,login,email,uprawnienia from user");
            tablePerson1.setItems(tablePersonData1);

        } catch (SQLException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void clickedTableUser(MouseEvent event) {
        try {
            String imie = tablePerson1.getSelectionModel().getSelectedItem().getName();
            String nazwisko = tablePerson1.getSelectionModel().getSelectedItem().getSurname();
            String login = tablePerson1.getSelectionModel().getSelectedItem().getLogin();
            String email = tablePerson1.getSelectionModel().getSelectedItem().getEmail();
            String funkcja = tablePerson1.getSelectionModel().getSelectedItem().getFunkction();

            namePerson.setText(imie);
            surnamePerson.setText(nazwisko);
            loginPerson.setText(login);
            //passwordPerson.setText(name);
            emailPerson.setText(email);
            passwordPerson.setText("");

            chChoiceFunction.setValue(funkcja);

        } catch (NullPointerException ex) {

        }
    }

    @FXML
    public void removePerson() {
        Connect connect = new Connect();

        String imie = namePerson.getText();
        String nazwisko = surnamePerson.getText();
        String login = loginPerson.getText();
        String email = emailPerson.getText();
        String funkcja = chChoiceFunction.getSelectionModel().getSelectedItem().toString();
        String haslo = passwordPerson.getText();

        String sql = ("Select haslo from user where imie=" + "'" + imie + "'" + " and nazwisko=" + "'" + nazwisko + "'" + " and login=" + "'" + login + "'" + " and email=" + "'" + email + "'" + " and uprawnienia=" + "'" + funkcja + "'");

        String validatepassword = connect.Select(sql);

        if (haslo.equals(validatepassword)) {
            if (login.equals("admin")) {
                pow.errorNotification(resourceBundle.getString("problemDeleteAdmin"));
            } else {
                String usuniecie = ("Delete from user where login=" + "'" + login + "'");
                connect.Delete(usuniecie);
                reloadTablePerson1();
            }
        } else {
            pow.warningNotification(resourceBundle.getString("failurePassword"));
        }

    }

    @FXML
    public void anuluj() {
        namePerson.setText("");
        surnamePerson.setText("");
        loginPerson.setText("");
        emailPerson.setText("");
        passwordPerson.setText("");
    }

    @FXML
    public void edytuj() {
        Connect connect = new Connect(resourceBundle);
        String imie = namePerson.getText();
        String nazwisko = surnamePerson.getText();
        String login = loginPerson.getText();
        String email = emailPerson.getText();
        String funkcja = chChoiceFunction.getSelectionModel().getSelectedItem().toString();
        String haslo = passwordPerson.getText();

        if (haslo.equals("")) {
            pow.warningNotification(resourceBundle.getString("editPerson"));
        } else {
            String loginWhere = login;
            connect.UpdatePerson(imie, nazwisko, login, haslo, email, funkcja, loginWhere);
        }
        reloadTablePerson1();
    }

    /**
     * Załadowanie rekordów do tabeli przy uruchomieniu Aplikacji
     *
     * @see Connect#selectTable(java.lang.String, java.lang.String)
     * @see Connect#getPersonTask(java.lang.String)
     *
     */
    public void reloadDatabase() {
        Connect con = new Connect();
        projects = FXCollections.observableArrayList();
        try {
            tableProjectData = con.selectTable("select nazwa, aktywnosc, date_from, date_to from projekt", "projekt");
            tableTaskData = con.getPersonTask("select nazwa_zadania, kto_wykonuje, wrealizacji, priorytet, data_from, data_to, procentowe_ukonczenie "
                    + "from zadania");
            tableMilestoneData = con.selectTable("select nazwa, odpowiedzialnosc, date_from , date_to, postep from kamienie_milowe; ", "kamienie_milowe");
            //dodanie aktualnych projektow do observ lista projects
            map = con.select("select id_projekt, nazwa from projekt");

            map.values().stream().forEach((String key) -> {
                projects.add(key);
            });

            tableKM.setItems(tableMilestoneData);
            tableProject.setItems(tableProjectData);
            chbProjects.setItems(projects);
            chbProjects.getSelectionModel().selectFirst();
            chChooseProjRap.setItems(projects);
            chChooseProjRap.getSelectionModel().selectFirst();
            cbProjectKM.setItems(projects);
            cbProjectKM.getSelectionModel().selectFirst();
            tableTask.setItems(tableTaskData);
            tableKM.setItems(tableMilestoneData);

        } catch (SQLException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //metoda usuwajaca aktualne elemetny w observable list
    /*public void clearObservableList() {
     projects.clear();
     chbProjects.getSelectionModel().selectFirst();
     chChooseProjRap.getSelectionModel().selectFirst();
     cbProjectKM.getSelectionModel().selectFirst();
     }*/
    /**
     * Initializowanie zakładki Projekt
     *
     * @see Permissions#Permissions(javafx.scene.control.Tab,
     * javafx.scene.control.Tab, javafx.scene.control.Button,
     * javafx.scene.control.Button, javafx.scene.control.DatePicker,
     * javafx.scene.control.DatePicker, javafx.scene.control.DatePicker,
     * javafx.scene.control.DatePicker, javafx.scene.control.TextField,
     * javafx.scene.control.TextArea, javafx.scene.control.ListView,
     * javafx.scene.control.ChoiceBox, javafx.scene.control.CheckBox,
     * javafx.scene.control.ComboBox, javafx.scene.control.TextField,
     * javafx.scene.control.ComboBox, javafx.scene.control.Button,
     * javafx.scene.control.Button, javafx.scene.control.TableView)
     * @see Permissions#selectModel()
     */
    public void initializeTableProjekt() {
        Image image = new Image("zdjecia/icon_project.png");
        ImageView iv = new ImageView(image);
        iconProjekty.setGraphic(iv);
        //model dla tabeli Projekty
        columnNameProject.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnDateStartProject.setCellValueFactory(new PropertyValueFactory<>("dateStart"));
        columnDateEndProject.setCellValueFactory(new PropertyValueFactory<>("dateEnd"));
        columnStateProject.setCellValueFactory(new PropertyValueFactory<>("status"));
        chState.setItems(stateCombo);
        chState.setValue("Aktywny");

        //odwolanie do klasy DatePickerLock i ustawienie formatu kalendarzu
        DatePickerLock lock = new DatePickerLock(dpStartDateProject, dpEndDateProject);
        lock.setCellFactory();

        /*if (projects.isEmpty()) {
         bRemove.setDisable(true);
         }*/
        Permissions permision = new Permissions(osobyTab, raportyTab, bSave, bAddTask, dpTaskSince, dpTaskTo, dpStartDateProject,
                dpEndDateProject, tfTaskName, taTaskNote, lvUsers, chbProjects, chbImplementation, cbPriorytet, tfNameProject, chState, bRemove, bEdit, tableProject);
        permision.selectModel();

    }

    /**
     * Initializowanie zakładki Zadania
     *
     * @see Connect#select(java.lang.String)
     * @see Connect#selectTable(java.lang.String, java.lang.String)
     * @see Task#deleteTask(java.lang.String)
     * @see Task#setProgressTask(java.lang.String, java.lang.String)
     * @see Task#editTask(java.lang.String, java.lang.String, java.lang.String)
     */
    public void initializeTableTask() {
        Connect con = new Connect();
        Image image = new Image("zdjecia/icon_task.png");
        ImageView iv = new ImageView(image);
        taskTab = new Task(resourceBundle);

        ActiveUser activeUser = new ActiveUser();
        String uprawnienia = activeUser.getUprawnienia();
        DatePickerLock lock = new DatePickerLock(dpTaskSince, dpTaskTo);

        columnTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
        columnInvolving.setCellValueFactory(new PropertyValueFactory<>("whoDoIt"));
        columnPriorytet.setCellValueFactory(new PropertyValueFactory<>("priorytet"));
        columnImplementation.setCellValueFactory(new PropertyValueFactory<>("implementation"));
        columnDateStartTask.setCellValueFactory(new PropertyValueFactory<>("strStartTask"));
        columnDateEndTask.setCellValueFactory(new PropertyValueFactory<>("strEndTask"));
        columnProcentTask.setCellValueFactory(new PropertyValueFactory<>("progresTask"));

        iconTask.setGraphic(iv);

        cbPriorytet.setItems(priorytetCombo);
        cbPriorytet.setValue("Średni");

        if (projects.isEmpty()) {

        } else {
            chbProjects.getSelectionModel().selectFirst();
            String usingProject = chbProjects.getSelectionModel().getSelectedItem().toString();

            for (Entry<String, String> entry : map.entrySet()) {
                if (usingProject.equals(entry.getValue())) {
                    idProjekt = Integer.parseInt(entry.getKey());
                }
            }
            System.out.println("Id-- " + idProjekt);
            lock.setCellFactoryTaskAndMilestone(idProjekt);
        }

        chbProjects.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                for (Entry<String, String> entry : map.entrySet()) {
                    if (entry.getValue().equals(newValue)) {
                        idProjekt = Integer.parseInt(entry.getKey());
                        initWykres();
                        lock.setCellFactoryTaskAndMilestone(idProjekt);
                    }
                }
            }
        });

        //dodanie elementów jako checkbox
        lvUsers.setItems(listUsers);
        lvUsers.setCellFactory(CheckBoxListCell.forListView((String item) -> {
            SimpleBooleanProperty observable = new SimpleBooleanProperty();
            observable.addListener((obs)
                    -> nameUser.add(item));
            return observable;
        }));

        /**
         * Poniższe instrukcje służące do wywołania menu kontekstowego po
         * naciśnięciu prawego klawisza myszy w miejscu tabeli Zadania i
         * wybraniu usuń, która usuwa rekord
         */
        // A context menu for the table column cells:
        tableTask.setRowFactory((TableView<TaskGen> tableT) -> {
            //final ObservableList<MenuItem> context = FXCollections.observableArrayList();
            final TableRow<TaskGen> row = new TableRow<>();
            final ArrayList<MenuItem> arrayMenu = new ArrayList<>();
            final MenuItem removeMenuItem = new MenuItem("Usuń");
            final Menu setCompleteTaskPercent = new Menu("Ukończenie w %");
            final MenuItem showNote = new MenuItem("Zobacz nottkę");
            showNote.setOnAction(e -> System.out.println("jj"));
            //wypełnienie listy elementami MenuItem
            for (int i = 10; i <= 100; i = i + 10) {
                arrayMenu.add(new MenuItem(i + "%"));
            }

            setCompleteTaskPercent.getItems().addAll(arrayMenu);

            final ContextMenu contextMenu = new ContextMenu();

            //action event do usuwania
            removeMenuItem.setOnAction((ActionEvent event) -> {
                taskTab.deleteTask(row.getItem().getTitle());
                //clearObservableList();
                reloadDatabase();
            });

            //action event do każdego podmenu
            arrayMenu.stream().forEach((i) -> {
                i.setOnAction((ActionEvent e) -> {
                    progressTask = i.getText();
                    System.out.println(progressTask);
                    taskTab.setProgressTask(progressTask, pressedTask);
                    //initWykres();
                    //clearObservableList();
                    reload();
                });
            });

            //contextMenu.getItems().addAll(removeMenuItem, setCompleteTaskPercent, showNote);
            // Set context menu on row, but use a binding to make it only show for non-empty rows:
            row.setOnMouseClicked(event -> {
                if (event.getButton().equals(MouseButton.SECONDARY)) {
                    try {
                        final String note = String.valueOf(con.selectTable("select notatka from zadania where nazwa_zadania = '"
                                + row.getItem().getTitle() + "'", "note")).replace("[", "").replace("]", "");
                        if (note.isEmpty() || note.equals("") || note.equals("null")) {
                            showNote.setDisable(true);
                            contextMenu.getItems().setAll(removeMenuItem, setCompleteTaskPercent, showNote);

                            row.contextMenuProperty().bind(Bindings
                                    .when(row.emptyProperty())
                                    .then((ContextMenu) null)
                                    .otherwise(contextMenu)
                            );
                        } else {
                            showNote.setDisable(false);
                            //actionevent dla notatki
                            showNote.setOnAction((ActionEvent evente) -> {
                                pow.showNote(note);

                            });
                            contextMenu.getItems().setAll(removeMenuItem, setCompleteTaskPercent, showNote);
                            row.contextMenuProperty().bind(Bindings
                                    .when(row.emptyProperty())
                                    .then((ContextMenu) null)
                                    .otherwise(contextMenu)
                            );
                            //pow.showNote(note);
                        }
                    } catch (SQLException ex) {
                    }
                }
            });
            return row;
        });

        /*Permissions per = new Permissions(tableTask);
         per.selectModel();*/
        //sprawdzenie kto jest zalogowany i blokowanie edytowania zadan
        if (uprawnienia.equals("admin") || uprawnienia.equals("kierownik")) {
            tableTask.setEditable(true);

            //kolumna Tytuł
            columnTitle.setCellFactory(TextFieldTableCell.forTableColumn());
            columnTitle.setOnEditCommit((CellEditEvent<TaskGen, String> t) -> {
                final String titleOld, titleNew;
                titleOld = t.getOldValue();
                titleNew = t.getNewValue();

                taskTab.editTask(titleOld, titleNew, "nazwa_zadania");

            });

            //kolumna Kto wykonał
            columnInvolving.setCellFactory(TextFieldTableCell.forTableColumn());
            columnInvolving.setOnEditCommit((CellEditEvent<TaskGen, String> t) -> {
                final String whoDoitOld, whoDoItNew;
                whoDoitOld = t.getOldValue();
                whoDoItNew = t.getNewValue();

                taskTab.editTask(whoDoitOld, whoDoItNew, "kto_wykonuje");
            });

            //kolumna W realizacji
            columnImplementation.setCellFactory(TextFieldTableCell.forTableColumn());
            columnImplementation.setOnEditCommit((CellEditEvent<TaskGen, String> t) -> {
                final String implementationOld, implementationNew;
                implementationOld = t.getOldValue();
                implementationNew = t.getNewValue();

                taskTab.editTask(implementationOld, implementationNew, "wrealizacji");
            });

            //kolumna Priorytet
            columnPriorytet.setCellFactory(TextFieldTableCell.forTableColumn());
            columnPriorytet.setOnEditCommit((CellEditEvent<TaskGen, String> t) -> {
                final String priorytetOld, priorytetNew;
                priorytetOld = t.getOldValue();
                priorytetNew = t.getNewValue();

                taskTab.editTask(priorytetOld, priorytetNew, "priorytet");
            });

            //kolumna Rozpoczęcie zadania 
            columnDateStartTask.setCellFactory(TextFieldTableCell.forTableColumn());
            columnDateStartTask.setOnEditCommit((CellEditEvent<TaskGen, String> t) -> {
                final String startDateOld, startDateNew;
                startDateOld = t.getOldValue();
                startDateNew = t.getNewValue();

                taskTab.editTask(startDateOld, startDateNew, "data_from");
            });

            //kolumna Rozpoczęcie zadania 
            columnDateEndTask.setCellFactory(TextFieldTableCell.forTableColumn());
            columnDateEndTask.setOnEditCommit((CellEditEvent<TaskGen, String> t) -> {
                final String endDateOld, endDateNew;
                endDateOld = t.getOldValue();
                endDateNew = t.getNewValue();

                taskTab.editTask(endDateOld, endDateNew, "data_to");
            });

            //kolumna procentowe ukończenie Zadania
            columnProcentTask.setCellFactory(ProgressBarTableCell.<TaskGen>forTableColumn());
        } else {
            tableTask.setEditable(false);
        }

        reloadProjectItem();

        try {
            listUsers = con.selectTable("select login from user", "user");
        } catch (SQLException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Metoda initializująca Wykres kołowy ukończenia zadań
     */
    private void initWykres() {
        Label label1 = new Label("Wynierz projekt: ");
        HBox hBox1 = new HBox();
        HBox hBox = new HBox();
        ChoiceBox chbox = new ChoiceBox();
        chbox.setItems(projects);
        chbox.getSelectionModel().selectFirst();
        chbox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    hBox.getChildren().clear();
                    //pobiera wartość i wyświetla klucz z mapy
                    for (Entry<String, String> entry : map.entrySet()) {
                        if (entry.getValue().equals(newValue)) {
                            idProjekt = Integer.parseInt(entry.getKey());
                        }
                    }

                    Image image = new Image("zdjecia/icon_wykres.png");
                    ImageView iv = new ImageView(image);
                    iv.autosize();

                    Connect con = new Connect();
                    ActiveUser activeUser = new ActiveUser();
                    ObservableList<PieChart.Data> dataList = con.getChartData(activeUser.getLogin(), idProjekt);
                    PieChart pieChart = new PieChart(dataList);

                    iconWykres.setGraphic(iv);

                    String s;
                    if (!dataList.isEmpty()) {
                        s = activeUser.getLogin();
                        pieChart.setTitle(resourceBundle.getString("progresUser") + " " + s);
                        hBox.getStylesheets().clear();
                        hBox.getStylesheets().add(getClass().getResource("chart.css").toString());

                        hBox.setAlignment(Pos.CENTER);
                        hBox.getChildren().addAll(pieChart);

                    } else {
                        hBox.getChildren().add(new Label("Brak wyniku"));
                    }
                    rootWykres.setCenter(hBox);                   
                }
            }
        });
        hBox1.setSpacing(10.0);
        hBox1.setPadding(new Insets(10, 10, 0, 10));
        hBox1.getChildren().addAll(label1,chbox);
        

        Image image = new Image("zdjecia/icon_wykres.png");
        ImageView iv = new ImageView(image);
        iv.autosize();

        Connect con = new Connect();
        ActiveUser activeUser = new ActiveUser();
        ObservableList<PieChart.Data> dataList = con.getChartData(activeUser.getLogin(), idProjekt);
        PieChart pieChart = new PieChart(dataList);

        iconWykres.setGraphic(iv);

        String s;
        if (!dataList.isEmpty()) {
            s = activeUser.getLogin();
            pieChart.setTitle(resourceBundle.getString("progresUser") + " " + s);
            hBox.getStylesheets().clear();
            hBox.getStylesheets().add(getClass().getResource("chart.css").toString());

            hBox.setAlignment(Pos.CENTER);
            hBox.getChildren().addAll(pieChart);

        } else {
            hBox.getChildren().clear();
        }
        rootWykres.setCenter(hBox);
       
        rootWykres.setTop(hBox1);
    }

    /**
     * Metoda initializująca raporty
     *
     * @see Connect#select(java.lang.String)
     */
    private void initReports() {
        Image image = new Image("zdjecia/icon_raport.png");
        ImageView iv = new ImageView(image);

        raportyTab.setGraphic(iv);

        //aktualny element ChoiceBoxa
        chChooseProjRap.getSelectionModel().selectFirst();
        chChooseProjRap.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {

                    //pobiera wartość i wyświetla klucz z mapy
                    for (Entry<String, String> entry : map.entrySet()) {
                        if (entry.getValue().equals(newValue)) {
                            idProjekt = Integer.parseInt(entry.getKey());
                        }
                    }
                }
            }
        });

    }

    /**
     * Metoda wywołująca zapisanie zestawienia
     *
     * @param event - zdarzenie kliknięcia na przycisk Generuj pdf
     * @see Connect#selectTable(java.lang.String, java.lang.String)
     * @see Connect#getPersonTask(java.lang.String)
     * @see Raports#setProjekt(java.lang.String)
     * @see Raports#createPdf()
     */
    @FXML
    void createReport(ActionEvent event) {
        Connect con = new Connect();
        ObservableList<ProjectGen> projectData = FXCollections.observableArrayList();
        ObservableList<TaskGen> taskData = FXCollections.observableArrayList();
        try {
            projectData = con.selectTable("select nazwa, aktywnosc, date_from, date_to from projekt where nazwa='"
                    + chChooseProjRap.getSelectionModel().getSelectedItem().toString() + "'", "projekt");

            taskData = con.getPersonTask("select nazwa_zadania, kto_wykonuje, wrealizacji, priorytet, data_from, data_to, procentowe_ukonczenie "
                    + "from zadania where projekt_id_projekt=" + idProjekt);

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        Raports rap = new Raports(chChooseProjRap, projectData, taskData, resourceBundle);
        rap.setProjekt(chChooseProjRap.getId());
        rap.createPdf();

    }

    /**
     * Metoda wylogowuje aktualnie zalogowanego użytkownika
     *
     * @param event - zdarzenie kliknięcia na przycisk Wyloguj w Menu
     * @see JavaFXApplication1#start(javafx.stage.Stage)
     */
    @FXML
    void logout(ActionEvent event) {
        stage.close();
        JavaFXApplication1 jfx = new JavaFXApplication1();
        try {
            jfx.start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Zamknięcie apliakcji
     *
     * @param event - zdarzenie zamknięcia okna aplikacji
     */
    @FXML
    void exitApp(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Okno informacyne o zespole MANAGAMENTOS
     *
     * @param event - zdarzenie kliknięcia na pozycje w menu Autorzy
     * @see Powiadomienia#aboutApplicaion()
     */
    @FXML
    void aboutApp(ActionEvent event) {
        //pow = new Powiadomienia(resourceBundle);
        pow.aboutApplicaion();
    }

    @FXML
    public void podrecznik() {
        try {
            Desktop desktop = Desktop.getDesktop();
            File file = new File("danedostepowe\\ManaGementos.pdf");
            desktop.open(file);
        } catch (Exception ex) {
            System.out.println("Brak błędów");
        }
    }

    /**
     * Metoda dodająca projekty to comboboxa w zakładce Zadania
     */
    void reloadProjectItem() {
        //aktualny element ChoiceBoxa
        chbProjects.getSelectionModel().selectFirst();
        chbProjects.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    tfTaskName.setDisable(false);
                    taTaskNote.setDisable(false);
                    lvUsers.setDisable(false);
                    chbImplementation.setDisable(false);
                    cbPriorytet.setDisable(false);
                    dpTaskSince.setDisable(false);
                    dpTaskTo.setDisable(false);

                    //pobiera wartość i wyświetla klucz z mapy
                    for (Entry<String, String> entry : map.entrySet()) {
                        if (entry.getValue().equals(newValue)) {
                            idProjekt = Integer.parseInt(entry.getKey());
                        }
                    }
                }
            }
        });

    }

    @FXML
    void addMilestone(ActionEvent event) {
        String nazwaKM = tfNazwaKamienia.getText();
        LocalDate start = dpStartKM.getValue();
        LocalDate koniec = dpKoniecKM.getValue();
        Connect con = new Connect();
        ObservableList<ProjectGen> projectData = FXCollections.observableArrayList();

        try {
            projectData = con.selectTable("select nazwa, aktywnosc, date_from, date_to from projekt where id_projekt="
                    + idProjekt, "projekt");
        } catch (SQLException se) {

        }
        String users = "";
        for (String user : nameUser) {
            if (user.equals("")); else {
                users = users + user + ", ";
            }
        }
        //System.out.println(nazwaKM + " \n" + start + " \n" +koniec + " ####"+ users);
        MilestoneGen msg = new MilestoneGen(nazwaKM, users, start, koniec, (short) 0);
        milestone = new Milestone(resourceBundle);
        //msg.toString();
        if (milestone.checkMilestone(msg, tableMilestoneData, projectData.get(0).getDateStart(), projectData.get(0).getDateEnd())) {
            milestone.addMilestone(msg, idProjekt);
        }
        reloadMilestone();
    }

    @FXML
    public void deleteMilestone(ActionEvent event) {
        milestone = new Milestone(resourceBundle);
        try {
            milestone.deleteMilestone(selectedMilestone);
        } catch (NullPointerException npe) {
            pow.warningNotification(resourceBundle.getString("checkRecord"));
        }
        reloadMilestone();
        selectedMilestone = null;
    }

    @FXML
    void clickedTableMilestone(MouseEvent event) {
        try {
            selectedMilestone = tableKM.getSelectionModel().getSelectedItem().getNazwa();
        } catch (NullPointerException npe) {

            selectedMilestone = null;

        }
    }

    public void initializeMilestone() {
        Image image = new Image("zdjecia/icon_milestone.png");
        ImageView iv = new ImageView(image);
        Connect con = new Connect();
        DatePickerLock lock = new DatePickerLock(dpStartKM, dpKoniecKM);
        milestone = new Milestone(resourceBundle);
        iconMilestone.setGraphic(iv);

        cbProjectKM.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    //pobiera wartość i wyświetla klucz z mapy
                    for (Entry<String, String> entry : map.entrySet()) {
                        if (entry.getValue().equals(newValue)) {
                            idProjekt = Integer.parseInt(entry.getKey());
                            lock.setCellFactoryTaskAndMilestone(idProjekt);
                        }
                    }
                }
            }
        });

        colNazwa.setCellValueFactory(new PropertyValueFactory<>("nazwa"));
        colOdpowiedzialni.setCellValueFactory(new PropertyValueFactory<>("odpowiedzialni"));
        colStart.setCellValueFactory(new PropertyValueFactory<>("startKM"));
        colEnd.setCellValueFactory(new PropertyValueFactory<>("endKM"));
        colPostep.setCellValueFactory(new PropertyValueFactory<>("postep"));

        tableKM.setRowFactory((TableView<MilestoneGen> tableK) -> {
            //final ObservableList<MenuItem> context = FXCollections.observableArrayList();
            final TableRow<MilestoneGen> row = new TableRow<>();
            final ArrayList<MenuItem> arrayMenu = new ArrayList<>();
            final Menu setCompleteTaskPercent = new Menu("Ukończenie w %");
            //wypełnienie listy elementami MenuItem
            for (int i = 10; i <= 100; i = i + 10) {
                arrayMenu.add(new MenuItem(i + "%"));
            }

            setCompleteTaskPercent.getItems().addAll(arrayMenu);

            final ContextMenu contextMenu = new ContextMenu();

            //action event do każdego podmenu
            arrayMenu.stream().forEach((i) -> {
                i.setOnAction((ActionEvent e) -> {
                    progressMilestone = i.getText();
                    milestone.setProgressMS(progressMilestone, selectedMilestone);
                    reloadDatabase();

                });
            });

            contextMenu.getItems().addAll(setCompleteTaskPercent);

            // Set context menu on row, but use a binding to make it only show for non-empty rows:
            row.contextMenuProperty().bind(Bindings
                    .when(row.emptyProperty())
                    .then((ContextMenu) null)
                    .otherwise(contextMenu)
            );
            return row;
        });
        colPostep.setCellFactory(ProgressBarTableCell.<MilestoneGen>forTableColumn());

        try {
            listUsers = con.selectTable("select login from user", "user");
            lvOdpowiedzialni.setItems(listUsers);
            lvOdpowiedzialni.setCellFactory(CheckBoxListCell.forListView((String item) -> {
                SimpleBooleanProperty observable = new SimpleBooleanProperty();
                observable.addListener((obs)
                        -> nameUser.add(item));
                return observable;
            }));
        } catch (SQLException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void reloadMilestone() {
        Connect con = new Connect();
        try {
            tableMilestoneData = con.selectTable("select nazwa, odpowiedzialnosc, date_from , date_to, postep from kamienie_milowe; ", "kamienie_milowe");
            tableKM.setItems(tableMilestoneData);
        } catch (SQLException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
