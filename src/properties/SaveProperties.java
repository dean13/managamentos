/*
 * Copyright (C) 2014 Łukasz Byjoś <lukasz.byjos@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package properties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;



public class SaveProperties {

    public final static String HOMESOURCE = System.getProperty("user.home") + System.getProperty("file.separator") + "Documents" + System.getProperty("file.separator") + "ManaGementos" + System.getProperty("file.separator");

    /**
     * Zapis ustawien do wybranego pliku w wybranej sciezce. komentarz dowolny
     * nazwa pliku tez sciezka bez \ lub /
     *
     * @param prop
     * @param komentarz
     * @param fileName
     * @param patch bez \ lub /
     * @throws java.io.FileNotFoundException
     */
    public void zapisz(Properties prop, String komentarz, String patch, String fileName) throws FileNotFoundException, IOException {
        File file = new File(patch);
        if (!file.exists()) {
            file.mkdirs();
        }
        file = new File(patch + System.getProperty("file.separator") + fileName);
        FileOutputStream fos = new FileOutputStream(file);
        prop.store(fos, komentarz);

    }
}
