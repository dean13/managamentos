/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainWindow;

import Baza.Connect;
import Generic.TaskGen;
import Powiadomienia.Powiadomienia;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;

/**
 * Klasa do zarządzania zakładką Zadania
 *
 * @author Alan
 */
public class Task {

    /**
     * Instancja klasy Powiadomienia
     */
    public Powiadomienia pow;
    public ResourceBundle resourceBundle;

    public Task(ResourceBundle rb) {
        
        this.resourceBundle = rb;
        this.pow = new Powiadomienia(rb);
    }
    
    public Task () {
        
    }

    /**
     * Metoda addTask - dodaje nowe zadanie
     *
     * @param g - odwołanie do konstruktora klasy generycznej TaskGen jako
     * argument
     * @param idprojekt - idprojektu podane jako argument
     * @see Baza.Connect#EasyInsert(java.lang.String)
     */
    public void addTask(TaskGen g, int idprojekt) {
        try {
            Connect.EasyInsert("INSERT INTO zadania (projekt_id_projekt ,nazwa_zadania,notatka,kto_wykonuje,wrealizacji,priorytet,data_from,data_to) VALUES ("
                    + "'" + idprojekt + "', '" + g.getTitle() + "', '" + g.getNote() + "', '" + g.getWhoDoIt() + "', '" + g.isImplementation() + "',"
                    + " '" + g.getPriorytet() + "', '" + g.getStartTask() + "', '" + g.getEndTask() + "')");
            pow.informationNotification(resourceBundle.getString("addTaskOK"));
        } catch (Exception ex) {
            pow.warningNotification(resourceBundle.getString("addTaskNO"));
        }
    }

    /**
     * Metoda deleteTas - usuwa zadanie
     *
     * @param name - nazwa zadania podana jako argument
     * @see Baza.Connect#Delete(java.lang.String)
     */
    public void deleteTask(String name) {
        Connect con = new Connect();
        if (pow.confirmDeleteRecord() == true) {
            con.Delete("DELETE FROM zadania WHERE nazwa_zadania='" + name + "'");
        }

    }

    /**
     * Metoda editTask - edytuje zadanie
     *
     * @param oldValue - wartość przed edycją danej komórki
     * @param newValue - wartość po zaaktualizowaniu danej komórki
     * @param column - nazwa edytowanej kolumny
     * @see Baza.Connect#Update(java.lang.String)
     */
    public void editTask(String oldValue, String newValue, String column) {
        Connect con = new Connect();

        try {
            con.Update("UPDATE zadania SET " + column + "='" + newValue + "' WHERE " + column + "='" + oldValue + "'");
        } catch (SQLException ex) {
            pow.warningNotification(resourceBundle.getString("editTask"));
        }
    }

    /**
     * Metoda setProgressTask ustawiająca procentowe ukończenie danego zadania
     *
     * @param value - wartość ukończenia zadania w procentach
     * @param title - tytuł zadania
     * @see Baza.Connect#Update(java.lang.String)
     */
    public void setProgressTask(String value, String title) {
        Connect con = new Connect();

        try {
            con.Update("UPDATE zadania SET procentowe_ukonczenie ='" + value + "' WHERE nazwa_zadania ='" + title + "'");
        } catch (SQLException ex) {
            pow.warningNotification(resourceBundle.getString("updateTask"));
        }
    }

    /**
     * Metoda validateTask walidująca poszczególne pola w zakładce Zadania
     *
     * @param g - odwołanie do konstruktora klasy generycznej TaskGen jako
     * argument
     * @param obsData - zmienna typu ObservableList przechowująca informacje o
     * Zadaniach
     * @param idProjekt - idProjektu
     * @return - metoda zwracająca typ logiczny
     */
    public boolean validateTask(TaskGen g, ObservableList<TaskGen> obsData, int idProjekt) {
        boolean exist = false;

        if (g.getTitle() == null || g.getWhoDoIt().equals("") || g.getStartTask() == null
                || g.getEndTask() == null || g.getPriorytet() == null) {
            pow.warningNotification(resourceBundle.getString("fillAllFieldsExceptNote"));
            return false;
        } else if (exist == true) {
            pow.warningNotification(resourceBundle.getString("alreadyExist"));
            return false;
        } else {
            return true;
        }
    }

}
