/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainWindow;

import Generic.ProjectGen;
import Generic.TaskGen;
import Powiadomienia.Powiadomienia;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Klasa generujaca raporty w formacie pdf
 *
 * @author Kamil Żaczek
 */
public class Raports {

    private String projekt;

    private final Powiadomienia powiadomienia;
    private final ChoiceBox chChooseProjRap;
    private ProjectGen proj;
    private TaskGen task;
    private ObservableList<ProjectGen> projectData = FXCollections.observableArrayList();
    private ObservableList<TaskGen> taskData = FXCollections.observableArrayList();
    private final ResourceBundle resourceBundle;

    public Raports(ChoiceBox chosRep, ObservableList<ProjectGen> pd, ObservableList<TaskGen> td, ResourceBundle rb) {
        this.chChooseProjRap = chosRep;
        this.projectData = pd;
        this.taskData = td;
        this.resourceBundle = rb;
        this.powiadomienia = new Powiadomienia(rb);
    }

    /**
     * Setter
     *
     * @param projekt - ?
     */
    public void setProjekt(String projekt) {
        this.projekt = projekt;
    }

    /**
     * Metoda pobierająca wszystkie zadania dla danego projektu
     *
     * @return zadania
     */
    public String taskForProject() {
        StringBuilder sb = new StringBuilder();
        try {
            for (TaskGen taskData1 : taskData) {
                sb.append("\n\nNazwa : " + taskData1.getTitle() + " \n");
                sb.append("   Priorytet: " + taskData1.getPriorytet() + " \n");
                sb.append("   Data rozpoczecia: " + taskData1.getStrStartTask() + "\n");
                sb.append("   Data zakonczenia: " + taskData1.getStrEndTask() + "\n");
                if (taskData1.getNote() == null) {
                    sb.append("   Notatki: Brak Notatki" + " \n");
                } else {
                    sb.append("   Notatki: " + taskData1.getNote() + " \n");
                }
                sb.append("   Wykonywane przez: " + taskData1.getWhoDoIt());

            }

            return sb.toString();
        } catch (NullPointerException npe) {
            return "BRAK ZADAN DO PROJEKTU";
        }

    }

    /**
     * Metoda przydotowująca dane które mają być przekazane do pliku PDF
     *
     * @return - zwraca informację o braku danych
     */
    public String prepareInfo() {
        try {
            if (!projectData.isEmpty()) {
                return " \n Status:" + projectData.get(0).getStatus()
                        + " \n Data rozpoczęcia: " + projectData.get(0).getDateStart().toString()
                        + " \n Data zakończenia: " + projectData.get(0).getDateEnd().toString()
                        + " \n \n \n    Zadania dla projektu: " + taskForProject();
            } else {
                return "Brak danych";
            }
        } catch (NullPointerException npe) {
            return "BRAK DANYCH ";
        }
    }

    /**
     * Metoda tworzaca dokument PDF
     */
    public void createPdf() {
        String filename;
        JFrame parentFrame = new JFrame();

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to save");
        fileChooser.setFileFilter(new FileNameExtensionFilter("PDF Files", "pdf"));
        int userSelection = fileChooser.showSaveDialog(parentFrame);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            try {
                filename = fileChooser.getSelectedFile().getPath() + ".pdf";
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream(filename));
                BaseFont font = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);
                Font arial = new Font(font, 14);
                Font arial1 = new Font(font, 18);
                document.open();
                Image image1 = Image.getInstance("src/zdjecia/logo2.gif");
                document.add(image1);
                Paragraph ppp1 = new Paragraph(projectData.get(0).getName(), arial1);
                ppp1.setAlignment(Element.ALIGN_CENTER);
                document.add(ppp1);
                document.add(new Paragraph(prepareInfo(), arial));
                document.close();
                powiadomienia.informationNotification(resourceBundle.getString("reportCreate")+" " + filename);
                try {
                    Desktop desktop = Desktop.getDesktop();
                    File file = new File(filename);
                    desktop.open(file);
                } catch (IOException ex) {
                    System.err.println(ex.getMessage());
                }
            } catch (IOException | DocumentException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
