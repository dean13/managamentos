/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainWindow;

import Generic.NewUser;
import Baza.Connect;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Klasa dla zakładki Osoby
 *
 * @author Kamil Wiśniewski
 */
public class Person {

    private final ResourceBundle resourceBundle;
    private final Connect con;
    
    public Person (ResourceBundle rb) {
        this.resourceBundle = rb;
        this.con = new Connect(rb);
    }
    /**
     * Metoda dodaje nowego użytkownika
     *
     * @param n - obiekt klasy NewUser
     */
    public void Add(NewUser n) {
        con.InsertPerson(n.getName(), n.getSurname(), n.getLogin(), n.getPassword(), n.getEmail(), n.getFunkction());

    }
    /**
     * Metoda usuwająca użytkownika o podanym loginie
     * @param login - parametr przekazujący login użytkownika do usunięcia
     * @throws SQLException 
     */
    public void delete(String login) throws SQLException {
        con.Delete("DELETE FROM user WHERE login = '" + login + "'");
    }

}
