/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainWindow;

import Baza.Connect;
import Generic.MilestoneGen;
import Powiadomienia.Powiadomienia;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;

/**
 * Klasa kamienie milowe
 *
 * @author Kamil Żaczek
 */
public class Milestone {

    public Powiadomienia powiadomienia;
    public ResourceBundle resourceBundle;
    
    public Milestone (ResourceBundle rb) {
        this.resourceBundle = rb;
        this.powiadomienia = new Powiadomienia(rb);
    }
/**
 * 
 * @param mg obiekt klasy generycznej 
 * @param idProj Id projektu 
 * metoda tworzy kamień milowy 
 */
    public void addMilestone(MilestoneGen mg, int idProj) {
        try {
            Connect.EasyInsert("INSERT INTO `kamienie_milowe`(`nazwa`, `projekt_id_projekt`, `date_from`, `date_to`, `odpowiedzialnosc`, `postep`) VALUES ('"
                    + mg.getNazwa() + "' , " + idProj + ", '" + mg.getStartKM() + "' , '" + mg.getEndKM() + "' , '" + mg.getOdpowiedzialni() + "' ," + mg.getPostepstr() + " )");
            powiadomienia.informationNotification(resourceBundle.getString("addMilestoneOK"));
        } catch (SQLException sq) {
            powiadomienia.warningNotification(resourceBundle.getString("addMilestoneNO"));
        } catch (NullPointerException npe) {
            powiadomienia.warningNotification(resourceBundle.getString("fillFields"));
        } catch (Exception ex) {
            Logger.getLogger(Milestone.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteMilestone(String nazwa) {
        Connect con = new Connect();
        try {
            con.Delete("DELETE FROM kamienie_milowe WHERE nazwa='" + nazwa + "'");
        } catch (Exception ex) {
            powiadomienia.warningNotification(ex.getMessage());
        }
    }

    public void setProgressMS(String value, String title) {
        Connect con = new Connect();
        try {
            con.Update("UPDATE kamienie_milowe SET postep ='" + value + "' WHERE nazwa ='" + title + "'");
        } catch (SQLException ex) {
            Logger.getLogger(Task.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean checkMilestone(MilestoneGen mg, ObservableList<MilestoneGen> mileData, LocalDate startPr, LocalDate endPr) {
        if (mg.getNazwa() == null || mg.getOdpowiedzialni().equals("")
                || mg.getStartKM() == null || mg.getEndKM() == null) {

            powiadomienia.warningNotification(resourceBundle.getString("fillFields"));
            return false;
        }
        if (mileData.contains(mg.getNazwa())) {
            powiadomienia.warningNotification(resourceBundle.getString("alreadyExist"));
            return false;
        }else {
            return true;
        }

    }

}
