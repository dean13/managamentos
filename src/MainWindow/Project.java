/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainWindow;

import Baza.Connect;
import Generic.ProjectGen;
import Powiadomienia.Powiadomienia;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Klasa do zarządzania zakładką Projekty
 *
 * @author Alan U
 */
public class Project {

    public Project() {

    }
    Powiadomienia pow;

    /**
     * Metoda saveProject - dodanie nowego Projektu w tabeli
     *
     * @param p - obiekt klasy generycznej ProjectGen
     * @see Baza.Connect#Insert(java.lang.String, java.lang.String,
     * java.time.LocalDate, java.time.LocalDate)
     */
    public void saveProject(ProjectGen p) {
        Connect con = new Connect();
        pow = new Powiadomienia(p.getResourceBundle());
        try {
            con.Insert(p.getName(), p.getStatus(), p.getDateStart(), p.getDateEnd());
            pow.informationNotification(p.getResourceBundle().getString("okSaveProject"));
        } catch (IOException ex) {
            pow.warningNotification(p.getResourceBundle().getString("failSaveProject"));
        }
    }

    /**
     * Metoda deleteProject do usunięcia rekordu z bazy, wywołana z argumentem
     * nazwa i instancją do klasy Conect
     *
     * @param name - usunięcie rekordu po nazwie poprzez argument
     * @see Baza.Connect#Delete(java.lang.String)
     */
    public void delateProject(String name) {
        Connect con = new Connect();
        con.Delete("DELETE FROM user809w_sss.projekt WHERE projekt.nazwa='" + name + "'");
    }

    /**
     * Metoda editProject do edytowania projektu
     *
     * @param nameOld - tytuł projektu przed edycją
     * @param name - tytuł projektu pod edycji
     * @param state - status projektu
     * @param dataStart - data rozpoczęcia projektu
     * @param dataEnd - data zakończenia projektu
     * @param rb - resource bundle
     * @see Baza.Connect#Update(java.lang.String)
     */
    public void editProject(String nameOld, String name, String state, LocalDate dataStart, LocalDate dataEnd, ResourceBundle rb) {
        Connect con = new Connect();
        pow = new Powiadomienia(rb);
        try {
            con.Update("UPDATE projekt SET nazwa='" + name + "', aktywnosc='" + state + "', date_from='" + dataStart + "', date_to='" + dataEnd + "'"
                    + " WHERE nazwa = '" + nameOld + "'");
            pow.informationNotification(rb.getString("okUpdateProject"));
        } catch (SQLException ex) {
            pow.warningNotification(rb.getString("failUpdateProject"));
        }

    }

    public boolean validateProject(ProjectGen p) {
        try {
            pow = new Powiadomienia(p.getResourceBundle());
            Connect con = new Connect();
            String title;
            
            title = String.valueOf(con.selectTable("select nazwa from projekt where nazwa='" + p.getName() + "'", "projekt-name")).replace("[", "").replace("]", "");
            System.out.println(p.getName() + " " + title);
            if (p.getName().isEmpty()) {
                pow.warningNotification(p.getResourceBundle().getString("fillFields"));
                return false;
            } else if (p.getName().length() > 30) {
                pow.warningNotification(p.getResourceBundle().getString("lengthProject"));
                return false;
            } else if (p.getName().equals(title)) {
                pow.warningNotification(p.getResourceBundle().getString("projectExist"));
                return false;
            }
            
        } catch (SQLException ex) {
        }
        return true;
    }
}
