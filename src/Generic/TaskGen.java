/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import java.time.LocalDate;

/**
 * Klasa generyczna zakładki Zadania
 *
 * @author Alan U
 */
public class TaskGen {

    /**
     * title (String) nazwa Zadania
     */
    public String title;
    /**
     * whoDoIt (String) kto wykonuje Zadanie
     */
    public String whoDoIt;
    /**
     * priorytet (String) priorytet Zadania
     */
    public String priorytet;
    /**
     * note (String) notatka do Zadania
     */
    public String note;
    /**
     * implementation (String) prechowuje informację o realizacji Zadania
     */
    public String implementation;
    /**
     * strStartTask (String) data rozpoczęcia Zadania
     */
    public String strStartTask;
    /**
     * strEndTask (String) data zakończenia Zadania
     */
    public String strEndTask;
    /**
     * startTask (LocalDate) data rozpoczęcia Zadania
     */
    public LocalDate startTask;
    /**
     * endTask (String) data zakończenia Zadania
     */
    public LocalDate endTask;
    private double progresTask;

    /**
     * Konstruktor dla obsługi klasy Task
     *
     * @param title - tytuł zadania
     * @param whoDoIt - kto wykonał zadanie
     * @param priorytet - priorytet zadania
     * @param note - notatka do zadania
     * @param implementation - status zadania
     * @param startTask - data rozpoczęcia zadania
     * @param endTask - data zakończenia zadania
     */
    public TaskGen(String title, String whoDoIt, String priorytet, String note, String implementation, LocalDate startTask, LocalDate endTask) {
        this.title = title;
        this.whoDoIt = whoDoIt;
        this.priorytet = priorytet;
        this.note = note;
        this.implementation = implementation;
        this.startTask = startTask;
        this.endTask = endTask;
    }

    /**
     * konstruktor dla Tabeli w GUI
     *
     * @param title - tytuł zadania
     * @param whoDoIt - kto wykonał zadanie
     * @param priorytet - priorytet zadania
     * @param implementation - status zadania
     * @param startTask - data rozpoczęcia zadania
     * @param endTask - data zakończenia zadania
     * @param progresProcentTask - procentowe ukończenie zadania
     */
    public TaskGen(String title, String whoDoIt, String priorytet, String implementation, String startTask, String endTask, int progresProcentTask) {
        this.title = title;
        this.whoDoIt = whoDoIt;
        this.priorytet = priorytet;
        this.implementation = implementation;
        this.strStartTask = startTask;
        this.strEndTask = endTask;
        this.progresTask = progresProcentTask;
    }

    /**
     * Domyślny konstruktor dla zakładni Zadania
     */
    public TaskGen() {

    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the whoDoIt
     */
    public String getWhoDoIt() {

        return whoDoIt;
    }

    /**
     * @param whoDoIt the whoDoIt to set
     */
    public void setWhoDoIt(String whoDoIt) {
        this.whoDoIt = whoDoIt;
    }

    /**
     * @return the priorytet
     */
    public String getPriorytet() {
        return priorytet;
    }

    /**
     * @param priorytet the priorytet to set
     */
    public void setPriorytet(String priorytet) {
        this.priorytet = priorytet;
    }

    /**
     * @return the implementation
     */
    public String isImplementation() {
        return implementation;
    }

    /**
     * @param implementation the implementation to set
     */
    public void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    /**
     * @return the startTask
     */
    public LocalDate getStartTask() {
        return startTask;
    }

    /**
     * @param startTask the startTask to set
     */
    public void setStartTask(LocalDate startTask) {
        this.startTask = startTask;
    }

    /**
     * @return the endTask
     */
    public LocalDate getEndTask() {
        return endTask;
    }

    /**
     * @param endTask the endTask to set
     */
    public void setEndTask(LocalDate endTask) {
        this.endTask = endTask;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the strStartTask
     */
    public String getStrStartTask() {
        return strStartTask;
    }

    /**
     * @param strStartTask the strStartTask to set
     */
    public void setStrStartTask(String strStartTask) {
        this.strStartTask = strStartTask;
    }

    /**
     * @return the strEndTask
     */
    public String getStrEndTask() {
        return strEndTask;
    }

    /**
     * @param strEndTask the strEndTask to set
     */
    public void setStrEndTask(String strEndTask) {
        this.strEndTask = strEndTask;
    }

    /**
     * @return the progresTask
     */
    public Double getProgresTask() {
        return progresTask / 100;
    }

    /**
     * @param progresTask the progresTask to set
     */
    public void setProgresTask(int progresTask) {
        this.progresTask = progresTask;
    }

}
