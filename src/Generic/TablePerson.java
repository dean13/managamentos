/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import java.time.LocalDate;

/**
 *
 * @author Kamil Wiśniewski
 */
public class TablePerson {
    /**
     * Prywatna zmienna login przechowująca loginy osób logowanych
     */
    private String login;
    /**
     * Prywatna zmienna nazwisko przechowująca nazwiska osób logowanych
     */
    private String nazwisko;
    /**
     * Prywatna zmienna data przechowująca daty logowań danych osób
     */
    private String data;

    /**
     * Konstruktor do inicjalizacji tabeli histori osób logowanych
     * @param login
     * @param nazwisko
     * @param data 
     */
    public TablePerson(String login, String nazwisko, String data) {
        this.login = login;
        this.nazwisko = nazwisko;
        this.data = data;
    }

    /**
     *
     * @return zwraca login zalogownego użytkownia
     */
    public String getLogin() {
        return login;
    }
    /**
     * 
     * @param login set login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     * @return zwraca nazwisko
     */
    public String getNazwisko() {
        return nazwisko;
    }
    
    /**
     * 
     * @param nazwisko set nazwisko 
     */
    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    /**
     *
     * @return zwraca date zalogowania użytkownika
     */
    public String getData() {
        return data;
    }
    /**
     * 
     * @param data set data 
     */
    public void setData(String data) {
        this.data = data;
    }

}
