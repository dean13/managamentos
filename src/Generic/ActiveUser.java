/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

/**
 *
 * @author Kamil
 */
public class ActiveUser {

    private static String login;
    private static String uprawnienia;
    private static String id_user;

    public static void setLogin(String login1) {
        login = login1;
    }
/**
 * 
 * @return zwraca login 
 */
    public String getLogin() {
        return login;
    }

    public static void setUprawnienia(String uprawnienia1) {
        uprawnienia = uprawnienia1;
    }
/**
 * 
 * @return zwraca uprawienia 
 */
    public String getUprawnienia() {
        return uprawnienia;
    }

    public static void setIdUser(String id_user1) {
        id_user = id_user1;
    }
/**
 * 
 * @return zwraca id_user 
 */
    public String getIdUser() {
        return id_user;
    }

}
