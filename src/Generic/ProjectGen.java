/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Klasa generyczna dla zakładki Projekty
 *
 * @author Alan U
 */
public class ProjectGen {

    /**
     * Prywatna zmienna name - przechowująca tytuł Projektu
     */
    private String name;
    /**
     * Prywatna zmienna status - przechowująca status Projektu
     */
    private String status;
    /**
     * Prywatna zmienna dateStart - przechowująca date rozpoczęcia Projektu
     */
    private LocalDate dateStart;
    /**
     * Prywatna zmienna dateEnd - przechowująca date zakończenia Projektu
     */
    private LocalDate dateEnd;
    
    private ResourceBundle resourceBundle;
    /**
     * Konstruktor dla obsłui tabeli Projekty
     *
     * @param name - nazwa projektu
     * @param status - status projektu
     * @param dateStart - data rozpoczęcia projektu
     * @param dateEnd - data zakończenia projektu
     */
    public ProjectGen(String name, String status, LocalDate dateStart, LocalDate dateEnd) {
        this.name = name;
        this.status = status;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }
    
    public ProjectGen (String name, String status, LocalDate dateStart, LocalDate dateEnd ,ResourceBundle rb) {
        this.name = name;
        this.status = status;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.resourceBundle = rb;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the dateStart
     */
    public LocalDate getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * @return the dateEnd
     */
    public LocalDate getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     * @return the resourceBundle
     */
    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    /**
     * @param resourceBundle the resourceBundle to set
     */
    public void setResourceBundle(ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

}
