/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

/**
 *
 * @author Kamil Wiśniewski
 */
public class NewUser {
    /**
     * prywatna zmienna name przyechowująca nazwe uzytkownika
     */
    private String name;
    /**
     * prywatna zmienna surname przyechowująca nazwisko uzytkownika
     */
    private String surname;
    /**
     * prywatna zmienna login przyechowująca login uzytkownika
     */
    private String login;
    /**
     * prywatna zmienna haslo przyechowująca haslo uzytkownika
     */
    private String password;
    /**
     * prywatna zmienna email przyechowująca email uzytkownika
     */
    private String email;
    /**
     * prywatna zmienna funkction przyechowująca uprwanienie uzytkownika
     */
    private String funkction;
    /**
     * prywatna zmienna date_logged przyechowująca date_logowania uzytkownika
     */
    private String date_logged;
    
 
    /**
     * Konstruktor NewUser do obsługi tabeli osoby
     * @param name -nazwa użytkownika
     * @param surname -naziwsko użytkownika
     * @param login - login użytkownika
     * @param password -hasło użytkownika
     * @param email -email użytkownika
     * @param funkction -funkcja użytkownika
     */
    public NewUser(String name, String surname, String login, String password, String email, String funkction) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.email = email;
        this.funkction = funkction;
    }

    // drugi konstruktor
    /**
     * Drugi konstruktor do obsługi tabeli z zakładki Osoby
     * @param name -imie
     * @param surname -nazwisko
     * @param login - login
     * @param email -email
     * @param funkction -uprawnienia 
     */
    public NewUser(String name, String surname, String login, String email, String funkction) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.email = email;
        this.funkction = funkction;
    }
    /**
     * 
     * @return date_logged
     */
    public String getDate_logged() {
        return date_logged;
    }

    /**
     * 
     * @param date_logged to set  
     */
    public void setDate_logged(String date_logged) {
        this.date_logged = date_logged;
    }
    
    /**
     *
     * @return zwraca użytkownika zalogowanego
     */
    public String getName() {
        return name;
    }
    /**
     * 
     * @param name set name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return zwraca Nazwisko użytkownika
     */
    public String getSurname() {
        return surname;
    }
    
    /**
     * 
     * @param surname set surname 
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     *
     * @return zwraca login uzytkownika
     */
    public String getLogin() {
        return login;
    }
    
    /**
     * 
     * @param login set login 
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     * @return zwraca hasło do użytkownika
     */
    public String getPassword() {
        return password;
    }
    /**
     * 
     * @param password set password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return zwraca email dla uzytkownika
     */
    public String getEmail() {
        return email;
    }
    /**
     * 
     * @param email set email 
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return zwraca funkcje dla użytkonika
     */
    public String getFunkction() {
        return funkction;
    }
    /**
     * 
     * @param funkction set funkction 
     */
    public void setFunkction(String funkction) {
        this.funkction = funkction;
    }

}
