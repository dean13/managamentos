package Generic;

import java.time.LocalDate;

/**
 * Klasa generyczna zakładki Kamienie milowe
 *
 * @author Kamil Żaczek
 */
public class MilestoneGen {

    public String nazwa;

    public String odpowiedzialni;

    public LocalDate startKM;

    public LocalDate endKM;

    public double postep;

    public MilestoneGen(String nazwa, String odpowiedzialni, LocalDate startKM, LocalDate endKM, short postep) {
        this.nazwa = nazwa;
        this.odpowiedzialni = odpowiedzialni;
        this.startKM = startKM;
        this.endKM = endKM;
        this.postep = postep;
    }
/**
 * 
 * @return zwraca nazwe osoby 
 */
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
/**
 * 
 * @return zwraca osobe odpowiedzialna za zadanie 
 */
    public String getOdpowiedzialni() {
        return odpowiedzialni;
    }

    public void setOdpowiedzialni(String odpowiedzialni) {
        this.odpowiedzialni = odpowiedzialni;
    }
/**
 * 
 * @return zwraca date początkową 
 */
    public LocalDate getStartKM() {
        return startKM;
    }

    public void setStartKM(LocalDate startKM) {
        this.startKM = startKM;
    }
/**
 * 
 * @return zwraca date końcową 
 */
    public LocalDate getEndKM() {
        return endKM;
    }

    public void setEndKM(LocalDate endKM) {
        this.endKM = endKM;
    }
/**
 * 
 * @return 
 */
    public Double getPostep() {
        return postep / 100;
    }
/**
 * 
 * @return 
 */
    public String getPostepstr() {
        return String.valueOf(postep);
    }

    public void setPostep(short postep) {
        this.postep = postep;
    }

    @Override
    public String toString() {
        return "MilestoneGen{" + "nazwa=" + nazwa + ", odpowiedzialni=" + odpowiedzialni + ", startKM=" + startKM + ", endKM=" + endKM + ", postep=" + postep + '}';
    }

}
