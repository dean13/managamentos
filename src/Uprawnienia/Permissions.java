package Uprawnienia;

import Generic.ActiveUser;
import Generic.ProjectGen;

import javafx.scene.control.Tab;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Klasa do uruchamiania kontrolek interfejsu wedlug uprawnien
 *
 * @author Kamil Żaczek
 */
public class Permissions {

    private final Tab osobyTab;
    private final Tab raportyTab;

    //pola do zadan
    private final Button bAddTask;
    private final TextField tfTaskName;
    private final TextArea taTaskNote;
    private final ListView<String> lvUsers;
    private final DatePicker dpTaskSince;
    private final DatePicker dpTaskTo;
    private final ChoiceBox chbProjects;
    private final CheckBox chbImplementation;
    private final ComboBox cbPriorytet;
    //pola do projektow
    private final Button bSave;
    private final DatePicker dpStartDateProject;
    private final DatePicker dpEndDateProject;
    private final TextField tfNameProject;
    private final ComboBox chState;
    private final Button bRemove;
    private final Button bEdit;
    private TableView<ProjectGen> tableProject;

    public Permissions(Tab ta, Tab tar, Button sav, Button add, DatePicker zadOd, DatePicker zadDo, DatePicker dpSDP, DatePicker dpEDP,
            TextField nazwZad, TextArea not, ListView<String> urz, ChoiceBox proj, CheckBox impl, ComboBox prio, TextField nazwProj,
            ComboBox stat, Button usunPro, Button edProj, TableView<ProjectGen> projekty) {
        this.osobyTab = ta;
        this.raportyTab = tar;
        this.bSave = sav;
        this.tfNameProject = nazwProj;
        this.chState = stat;
        this.bRemove = usunPro;
        this.bEdit = edProj;
        this.bAddTask = add;
        this.dpTaskSince = zadOd;
        this.dpTaskTo = zadDo;
        this.tfTaskName = nazwZad;
        this.taTaskNote = not;
        this.lvUsers = urz;
        this.chbProjects = proj;
        this.cbPriorytet = prio;
        this.chbImplementation = impl;
        this.dpStartDateProject = dpSDP;
        this.dpEndDateProject = dpEDP;
        this.tableProject = projekty;
    }

    /**
     * Metoda sprawdzająca jakie uprawnienia ma uzytkownik ktory sie loguje
     */
    public void selectModel() {
        ActiveUser activeUser = new ActiveUser();
        String uprawnienia = activeUser.getUprawnienia();

        switch (uprawnienia) {
            case "admin":
                admin();
                break;
            case "kierownik":
                kierownik();
                break;
            case "pracownik":
                pracownik();
                break;
            default:
                System.exit(0);
        }

    }

    /**
     * Uruchamianie odpowiednich kontrolek da admina
     */
    public void admin() {
        osobyTab.setDisable(false);
        raportyTab.setDisable(false);
        bSave.setDisable(false);
        dpTaskSince.setDisable(false);
        dpTaskTo.setDisable(false);
        tfTaskName.setDisable(false);
        taTaskNote.setDisable(false);
        lvUsers.setDisable(false);
        cbPriorytet.setDisable(false);
        chbImplementation.setDisable(false);
        chbProjects.setDisable(false);
        tfNameProject.setDisable(false);
        chState.setDisable(false);
        dpEndDateProject.setDisable(false);
        dpStartDateProject.setDisable(false);
        tableProject.setEditable(true);
    }

    /**
     * Uruchamianie odpowiednich kontrolek da kierownika
     */
    public void kierownik() {
        bSave.setDisable(false);
        bAddTask.setDisable(false);
        dpTaskSince.setDisable(false);
        dpTaskTo.setDisable(false);
        tfTaskName.setDisable(false);
        taTaskNote.setDisable(false);
        lvUsers.setDisable(false);
        cbPriorytet.setDisable(false);
        chbImplementation.setDisable(false);
        chbProjects.setDisable(false);
    }

    /**
     * Uruchamianie odpowiednich kontrolek dla pracownika
     */
    public void pracownik() {
        taTaskNote.setDisable(false);
    }

}
