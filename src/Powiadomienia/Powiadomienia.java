/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Powiadomienia;

import java.util.Optional;
import java.util.ResourceBundle;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Klasa wyświetlająca powiadomienia
 *
 * @author Alan U
 */
public class Powiadomienia {
    private ResourceBundle resourceBundle;
   
    
    public Powiadomienia (ResourceBundle rb) {
        this.resourceBundle = rb;
    }

    /**
     * Metoda publiczna wyświetlanie komunikatu o poprawnym usunięciu Rekordu.
     *
     * @return - zwraca komunikat jeśli zostanie wciśnięty przycisk OK
     */
    public boolean confirmDeleteRecord() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(resourceBundle.getString("delete"));
        alert.setHeaderText(null);
        alert.setContentText(resourceBundle.getString("confirmDelete"));
        alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    public boolean confirmExitAplication() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("MANAGEMETOS !");
        alert.setHeaderText(null);
        alert.setContentText(resourceBundle.getString("confirmExit"));
        alert.initStyle(StageStyle.UTILITY);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            return true;
        } else {
            alert.close();
            return false;
        }
    }

    /**
     * Metoda wyświetlająca komunikat informacyjny z zawartością otrzymaną jako
     * argument.
     *
     * @param text - informacja powiadomienia jaka argument
     */
    public void informationNotification(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(resourceBundle.getString("information"));
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.initStyle(StageStyle.UTILITY);
        alert.showAndWait();
    }
    
    /**
     * Metoda wyświetlająca komunikat informacyjny z zawartością otrzymaną jako
     * argument.
     *
     * @param text - informacja powiadomienia jaka argument
     */
    public void showNote(String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(resourceBundle.getString("showNote"));
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.initStyle(StageStyle.UTILITY);
        alert.showAndWait();
    }

    /**
     * Metoda wyświetlająca komunikat ostrzeżenia z zawartością otrzymaną jako
     * argument.
     *
     * @param text - informacja powiadomienia jaka argument
     */
    public void warningNotification(String text) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(resourceBundle.getString("information"));
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.initStyle(StageStyle.UTILITY);
        alert.showAndWait();
    }

    /**
     * Metoda wyświetlająca komunikat ostrzeżenia z zawartością otrzymaną jako
     * argument.
     *
     * @param text - informacja powiadomienia jaka argument
     */
    public void errorNotification(String text) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(resourceBundle.getString("error"));
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.initStyle(StageStyle.UTILITY);
        alert.showAndWait();
    }

    public void aboutApplicaion() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(resourceBundle.getString("aboutApp"));
        alert.setHeaderText(resourceBundle.getString("team"));
        alert.setContentText("\tAlan Urban\n\tKamil Wiśniewski\n\tKamil Żaczek\n\tKrzysztof Śmiertka\n\tMateusz Wójcik\n\t");
        alert.initStyle(StageStyle.UTILITY);
        alert.showAndWait();
    }

}
