/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import java.time.LocalDate;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kamill
 */
public class TaskGenTest {
    
    public TaskGenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    LocalDate start = LocalDate.of(2015, Month.MARCH, 10);
    LocalDate end = LocalDate.now();
    TaskGen instance = new TaskGen("zad", "admin", "wysoki", "zad", "zad", start, end);
    /**
     * Test of getTitle method, of class TaskGen.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        
        String expResult = "zad";
        String result = instance.getTitle();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getWhoDoIt method, of class TaskGen.
     */
    @Test
    public void testGetWhoDoIt() {
        System.out.println("getWhoDoIt");
                String expResult = "admin";
        String result = instance.getWhoDoIt();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getPriorytet method, of class TaskGen.
     */
    @Test
    public void testGetPriorytet() {
        System.out.println("getPriorytet");
        
        String expResult = "wysoki";
        String result = instance.getPriorytet();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of isImplementation method, of class TaskGen.
     */
    @Test
    public void testIsImplementation() {
        System.out.println("isImplementation");
        
        String expResult = "zad";
        String result = instance.isImplementation();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getStartTask method, of class TaskGen.
     */
    @Test
    public void testGetStartTask() {
        System.out.println("getStartTask");
        
        LocalDate expResult = start;
        LocalDate result = instance.getStartTask();
        assertEquals(expResult, result);
            }

    /**
     * Test of getEndTask method, of class TaskGen.
     */
    @Test
    public void testGetEndTask() {
        System.out.println("getEndTask");
        
        LocalDate expResult = end;
        LocalDate result = instance.getEndTask();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getNote method, of class TaskGen.
     */
    @Test
    public void testGetNote() {
        System.out.println("getNote");
        String expResult = "zad";
        String result = instance.getNote();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }


    /**
     * Test of getProgresTask method, of class TaskGen.
     */
    @Test
    public void testGetProgresTask() {
        System.out.println("getProgresTask");
        
        Double expResult = 0.0;
        Double result = instance.getProgresTask();
        assertEquals(expResult, result);
        
    }
    
}
