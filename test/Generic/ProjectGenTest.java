/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import java.time.LocalDate;
import java.time.Month;
import java.util.ResourceBundle;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kamill
 */
public class ProjectGenTest {
    
    public ProjectGenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    LocalDate start = LocalDate.of(2015, Month.MARCH, 10);
    LocalDate end = LocalDate.now();
    ProjectGen instance = new ProjectGen("proj", "proj", start, end);
    /**
     * Test of getName method, of class ProjectGen.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        
        String expResult = "proj";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of getStatus method, of class ProjectGen.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");
        String expResult = "proj";
        String result = instance.getStatus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
/**
     * Test of getDateStart method, of class ProjectGen.
     */
    @Test
    public void testGetDateStart() {
        System.out.println("getDateStart");
        
        LocalDate expResult = start;
        LocalDate result = instance.getDateStart();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
/**
     * Test of getDateEnd method, of class ProjectGen.
     */
    @Test
    public void testGetDateEnd() {
        System.out.println("getDateEnd");
        
        LocalDate expResult = end;
        LocalDate result = instance.getDateEnd();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    
}
