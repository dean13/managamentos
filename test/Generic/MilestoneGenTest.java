/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import java.time.LocalDate;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kamill
 */
public class MilestoneGenTest {
    
    public MilestoneGenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    LocalDate start = LocalDate.of(2015, Month.MARCH, 10);
    LocalDate end = LocalDate.now();
    MilestoneGen instance = new MilestoneGen("kamin", "admin", start, end, (short)10);
    /**
     * Test of getNazwa method, of class MilestoneGen.
     */
    @Test
    public void testGetNazwa() {
        System.out.println("getNazwa");
        
        String expResult = "kamin";
        String result = instance.getNazwa();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    /**
     * Test of getOdpowiedzialni method, of class MilestoneGen.
     */
    @Test
    public void testGetOdpowiedzialni() {
        System.out.println("getOdpowiedzialni");
        
        String expResult = "admin";
        String result = instance.getOdpowiedzialni();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getStartKM method, of class MilestoneGen.
     */
    @Test
    public void testGetStartKM() {
        System.out.println("getStartKM");
        
        LocalDate expResult = start;
        LocalDate result = instance.getStartKM();
        assertEquals(expResult, result);
        
    }


    /**
     * Test of getEndKM method, of class MilestoneGen.
     */
    @Test
    public void testGetEndKM() {
        System.out.println("getEndKM");
        
        LocalDate expResult = end;
        LocalDate result = instance.getEndKM();
        assertEquals(expResult, result);
        
    }


    /**
     * Test of getPostep method, of class MilestoneGen.
     */
    @Test
    public void testGetPostep() {
        System.out.println("getPostep");
        
        Double expResult = 0.1;
        Double result = instance.getPostep();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }


    
}
