/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kamill
 */
public class TablePersonTest {
    
    public TablePersonTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    TablePerson instance = new TablePerson("admin", "admin", "21-06-2015");
    /**
     * Test of getLogin method, of class TablePerson.
     */
    @Test
    public void testGetLogin() {
        System.out.println("getLogin");
        
        String expResult = "admin";
        String result = instance.getLogin();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNazwisko method, of class TablePerson.
     */
    @Test
    public void testGetNazwisko() {
        System.out.println("getNazwisko");
        String expResult = "admin";
        String result = instance.getNazwisko();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of getData method, of class TablePerson.
     */
    @Test
    public void testGetData() {
        System.out.println("getData");
        String expResult = "21-06-2015";
        String result = instance.getData();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
}
