/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Generic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kamill
 */
public class NewUserTest {
    
    public NewUserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    NewUser instance = new NewUser("admin","admin","admin","admin","admin","admin");
    /**
     * Test of getDate_logged method, of class NewUser.
     */

    /**
     * Test of getName method, of class NewUser.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = "admin";
        String result = instance.getName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getSurname method, of class NewUser.
     */
    @Test
    public void testGetSurname() {
        System.out.println("getSurname");
        String expResult = "admin";
        String result = instance.getSurname();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getLogin method, of class NewUser.
     */
    @Test
    public void testGetLogin() {
        System.out.println("getLogin");
        String expResult = "admin";
        String result = instance.getLogin();
        assertEquals(expResult, result);
        
    }


    /**
     * Test of getPassword method, of class NewUser.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        String expResult = "admin";
        String result = instance.getPassword();
        assertEquals(expResult, result);
        
    }


    /**
     * Test of getEmail method, of class NewUser.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        String expResult = "admin";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getFunkction method, of class NewUser.
     */
    @Test
    public void testGetFunkction() {
        System.out.println("getFunkction");
        String expResult = "admin";
        String result = instance.getFunkction();
        assertEquals(expResult, result);
        
    }
    
}
